/*
Tasks:
defult = debug
debug = playerDebugMode:set, clean:all, stage:extension
release = debug, sign, decompress
watch = debug, watch extension dir and run debug when changed

Test tasks:
test:install = release, symlink:clear, playerDebugMode:unset, copy:decompressed
test:uninstall = symlink:clear, symlink:make, playerDebugMode:set, debug

Build tasks:
clean:all = delete build dir
processBlockFiles = get names of blockfiles
stage:extension = replace tokens and copy all files to build dir

Symlink tasks:
symlink:make = symlink dest to AE cep folder in appdata
symlink:clear = clear symlink

Registry tasks:
playerDebugMode:set = set debug flag to 1
playerDebugMode:unset = set debug flag to 0

Release tasks:
sign = sign zxp file
decompress = decompress zxp file and place it in release dir
copy:decompressed = copy decompressed dir to symlink dir (CEP in appdata)

*/

const gulp = require("gulp");
const fs = require("fs");
const path = require("path");
const del = require("del");
const shell = require("gulp-shell");
const replace = require("gulp-token-replace");
const rseq = require("run-sequence");
const zxpSignCmd = require("zxp-sign-cmd");
const filter = require("gulp-filter");
const decompress = require("gulp-decompress");
const argv = require("yargs").argv;
const uglify = require('gulp-uglify')
const concat = require('gulp-concat');

const debug = argv.debug || false;

const pkg = JSON.parse(fs.readFileSync("./package.json"));
const EXTENSION_DIR = "./src/" + pkg.name;
const SRC_DIR = "./src/";
const STAGING_DIR = "./build/";
const RELEASE_NAME = pkg.name + "-v" + pkg.version;
const RELEASE_DIR = "./release/";
var jsBlocks, xmlBlocks;

const symlink = {
  "src": path.join(STAGING_DIR, pkg.name),
  "dest": path.join(process.env.APPDATA, "Adobe", "CEP", "extensions", pkg.name)
};

const zxp = {
  "src": path.join(STAGING_DIR, pkg.name),
  "dest": RELEASE_DIR + RELEASE_NAME + ".zxp"
};

const replaceToken = {
  "prefix": "B:R{{",
  "suffix": "}}",
  "global": {
    "pkg": pkg,
    "debug": debug,
    "jsBlocks": "",
    "xmlBlocks": ""
  }
};

/*** TOP-LEVEL TASKS ***/

gulp.task("default", ["debug"]);

gulp.task("debug", function(cb) {
  return rseq("playerDebugMode:set", "clean:all", "stage:processBlockFiles", "stage:concatBlocklyXML", "stage:extension", cb);
});

gulp.task("release", function(cb) {
  return rseq("debug", "sign", "decompress", cb);
});

gulp.task("watch", ["debug"], function() {
  console.log("Loading watch, please wait... this may take a while.");
  gulp.watch([
    EXTENSION_DIR + "*/**"
  ], ["debug"]);
  console.log("Watch ready!");
});

/*** TEST TASKS ***/

/*
 * This task is to 'install' the tool in a test environment.
 *
 * It will:
 *  - Build & compile a ZXP
 *  - remove any debug registry flags
 *  - clear the symlink
 *  - copy the decompressed folder to your extension dir
 */
gulp.task("test:install", ["release"], function(cb) {
  return rseq("symlink:clear", "playerDebugMode:unset", "copy:decompressed", cb);
});

/*
 * This task is to 'uninstall' the test environment
 *
 * It will:
 *  - Remove the decompressed folder
 *  - Rebuild the symlink
 *  - Enable registry flags
 *  - Build the script
 */
gulp.task("test:uninstall", function(cb) {
  return rseq("symlink:clear", "symlink:make", "playerDebugMode:set", "debug", cb);
});

/*** BUILD TASKS ***/

gulp.task("clean:all", function() {
  return del([
    STAGING_DIR
  ], {
    "force": true
  });
});

gulp.task("stage:processBlockFiles", function() {
  jsBlocks = "";
  fs.readdirSync(SRC_DIR + "/Blocky Developer Toolkit files/Block Exporter/").forEach(file => {
    if (file != "aaa_template.js") {
      jsBlocks += '<script src="js/blocks/' + file + '"></script>\n';
    }
  })
  replaceToken.global.jsBlocks = jsBlocks;

  var textFilter = filter(["**/*.{js,jsx,jsxinc,xml,json,html}", "!src/node_modules/**/*"], { "restore": true });

  return gulp.src([
      SRC_DIR + "/Blocky Developer Toolkit files/Block Exporter/*",
      "!" + SRC_DIR + "/Blocky Developer Toolkit files/Block Exporter/aaa_template.js",
    ])
    .pipe(textFilter)
    .pipe(replace(replaceToken))
    .pipe(textFilter.restore)
    .pipe(gulp.dest(STAGING_DIR + pkg.name + "/js/blocks"));

});

gulp.task("stage:concatBlocklyXML", function() {
  xmlBlocks = [];
  fs.readdirSync(SRC_DIR + "/Blocky Developer Toolkit files/Workingspace Factory").forEach(file => {
    xmlBlocks += fs.readFileSync(SRC_DIR + "/Blocky Developer Toolkit files/Workingspace Factory/" + file, "utf8").replace(new RegExp("<xml.*", "gm"), "").replace(new RegExp("</xml.*", "gm"), "");
  })
  replaceToken.global.xmlBlocks = xmlBlocks;
});

gulp.task("stage:extension", function() {
  var textFilter = filter(["**/*.{js,jsx,jsxinc,xml,json,html}", "!src/node_modules/**/*"], { "restore": true });

  return gulp.src([
      EXTENSION_DIR + "*/.debug",
      EXTENSION_DIR + "*/**",
      "!" + EXTENSION_DIR + "/xml",
      "!" + EXTENSION_DIR + "/xml/*"
    ])
    .pipe(textFilter)
    .pipe(replace(replaceToken))
    .pipe(textFilter.restore)
    .pipe(gulp.dest(STAGING_DIR));
});

/*** SYMLINK TASKS ***/

gulp.task("symlink:make", function() {
  var cmd = 'mklink /j "' + symlink.dest + '" "' + symlink.src + '"'; // eslint-disable-line quotes
  shell.task(cmd)();
});

gulp.task("symlink:clear", function() {
  console.log(symlink.dest);
  del(path.resolve(symlink.dest), {
    "force": true
  });
});

/*** REGISTRY TASKS ***/

gulp.task("playerDebugMode:set", function() {
  var commands = [
    "REG ADD HKCU\\Software\\Adobe\\CSXS.4 /f /v PlayerDebugMode /t REG_SZ /d 1",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.5 /f /v PlayerDebugMode /t REG_SZ /d 1",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.6 /f /v PlayerDebugMode /t REG_SZ /d 1",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.7 /f /v PlayerDebugMode /t REG_SZ /d 1",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.8 /f /v PlayerDebugMode /t REG_SZ /d 1"
  ].join(" && ");

  shell.task(commands)();
});

gulp.task("playerDebugMode:unset", function() {
  var commands = [
    "REG ADD HKCU\\Software\\Adobe\\CSXS.4 /f /v PlayerDebugMode /t REG_SZ /d 0",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.5 /f /v PlayerDebugMode /t REG_SZ /d 0",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.6 /f /v PlayerDebugMode /t REG_SZ /d 0",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.7 /f /v PlayerDebugMode /t REG_SZ /d 0",
    "REG ADD HKCU\\Software\\Adobe\\CSXS.8 /f /v PlayerDebugMode /t REG_SZ /d 0"
  ].join(" && ");

  shell.task(commands)();
});

/*** RELEASE TASKS ***/

gulp.task("sign", function(cb) {
  zxpSignCmd.sign({
    "input": zxp.src,
    "output": zxp.dest,
    "cert": "./publishercert.p12",
    "password": "buckla",
    "timestamp": "http://sha1timestamp.ws.symantec.com/sha1/timestamp"
  }, function(error, result) {
    if (error)
      console.log(error);

    console.log(result);
    cb();
  });
});

gulp.task("decompress", function() {
  return gulp.src([
      zxp.dest
    ])
    .pipe(decompress())
    .pipe(gulp.dest(RELEASE_DIR + RELEASE_NAME));
});

gulp.task("copy:decompressed", function() {
  return gulp.src([
      RELEASE_DIR + RELEASE_NAME + "/**",
      RELEASE_DIR + RELEASE_NAME + "/.debug"
    ])
    .pipe(gulp.dest(symlink.dest));
});