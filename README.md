# AE CodeBlocks

As I don't code much any longer for AE, even less create extensions, I haven't touched my CodeBlocks extension for way to long. If you haven't seen how it works, here is the current state of it: https://imgur.com/a/8Tim4NG
![gif](https://i.imgur.com/mrnb26m.gif)

As I see potential in the idea but won't continue with it, here is the current source code for anyone who want to use it or continue develop for it. Would love to see it out for free some day but if someone here wants to sell it, go nuts! Better that than nothing happens at all with it :)

It was quite a while ago I worked on it so I don't fully remember how it's set up but here is the structure from what I remember:

* The whole extension is based of Google's Blockly
* I used gulp to build the final extension
* Final build ends up in the build folder
* In the folde src you have this:
	- Blockly: This is the source code of Blockly that you can download from google's page. It might be old now and an update might be of interest but nothing critical.
	Here you have the Blockly Developer Tools where you generate the neeeded blockly files. You'll find it under src\Blockly\demos\blockfactory\index.html
	-  Blocky Developer Toolkit files: Here I place the generated filse from Blocklys block-generator that gulp later combines into one complete file that Blockly can read.
	Using batchProcess.jsx you can select .rst files from After Effects Scripting Guide to quickly generate the needed code from that segment of the docs for Blockly (check below)
	- closure-library: A library Blockly needs/needed
	- Codeblocks: The extension itself!
	- guide: After Effects Scripting Guide from http://docs.aenhancers.com/ dated from 2017/08/14 (yep, this script is that old...). From here I take all information to generate the blocks from.

That's all. Hope someone might find this interesting :)