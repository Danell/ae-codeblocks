// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_project_items_addcomp'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("add comp");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("width");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("PIXELASPECT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("pixel aspect ratio");
    this.appendValueInput("DURATION")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("duration in seconds");
    this.appendValueInput("FRAMERATE")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("frame rate");
    this.setOutput(true, null);
    this.setColour(20);
 this.setTooltip("Creates a new composition. Creates and returns a new CompItem object and adds it to this collection. If the ItemCollection belongs to the project or the root folder, then the new item’s parentFolder is the root folder. If the ItemCollection belongs to any other folder, the new item’s parentFolder is that FolderItem.");
 this.setHelpUrl("src/guide/index.html#itemcollection-addcomp");
  }
};

Blockly.Blocks['ae_project_items_addfolder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("add comp");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.setOutput(true, null);
    this.setColour(20);
 this.setTooltip("Creates a new folder. Creates and returns a new FolderItem object and adds it to this collection. If the ItemCollection belongs to the project or the root folder, then the new folder’s parentFolder is the root folder. If the ItemCollection belongs to any other folder, the new folder’s parentFolder is that FolderItem. To put items in the folder, set the Item.parentFolder attribute");
 this.setHelpUrl("guide/index.html#itemcollection-addfolder");
  }
};

Blockly.Blocks['ae_project_activeitem'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("active item");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The item that is currently active and is to be acted upon, or a null if no item is currently selected or if multiple items are selected.");
 this.setHelpUrl("guide/index.html#project-activeitem");
  }
};

Blockly.Blocks['ae_project_bitsperchannel'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("bits per channel (bcp)");
    this.setOutput(true, "Number");
    this.setColour(20);
 this.setTooltip("The color depth of the current project, either 8, 16, or 32 bits.");
 this.setHelpUrl("guide/index.html#project-bitsperchannel");
  }
};

Blockly.Blocks['ae_project_bitsperchannel_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set bits per channel (bcp) to")
        .appendField(new Blockly.FieldDropdown([["8","8"], ["16","16"], ["32","32"]]), "bpc");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The color depth of the current project, either 8, 16, or 32 bits.");
 this.setHelpUrl("guide/index.html#project-bitsperchannel");
  }
};

Blockly.Blocks['ae_project_feetframesfilmtype_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set feet frames film type to")
        .appendField(new Blockly.FieldDropdown([["16","FeetFramesFilmType.MM16"], ["32","FeetFramesFilmType.MM35"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The Use Feet + Frames menu setting in the Project Settings dialog box. Use this attribute instead of the old ``timecodeFilmType`` attribute.");
 this.setHelpUrl("guide/index.html#project-feetframesfilmtype");
  }
};

Blockly.Blocks['ae_project_file'] = {
  init: function() {
    this.appendValueInput("TYPE")
        .setCheck("File")
        .appendField("file");
    this.setOutput(true, null);
    this.setColour(20);
 this.setTooltip("The ExtendScript File object for the file containing the project that is currently open.");
 this.setHelpUrl("guide/index.html#project-file");
  }
};

Blockly.Blocks['ae_project_file_write'] = {
  init: function() {
    this.appendValueInput("TYPE")
        .setCheck("File object")
        .appendField("file");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The ExtendScript File object for the file containing the project that is currently open.");
 this.setHelpUrl("guide/index.html#project-file");
  }
};

Blockly.Blocks['ae_project_footagetimecodedisplaystarttype_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set footage timecode display start type to")
        .appendField(new Blockly.FieldDropdown([["source media","FootageTimecodeDisplayStartType.FTCS_USE_SOURCE_MEDIA"], ["0","FootageTimecodeDisplayStartType.FTCS_START_0"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The Footage Start Time setting in the Project Settings dialog box, which is enabled when Timecode is selected as the time display style.");
 this.setHelpUrl("guide/index.html#project-footagetimecodedisplaystarttype");
  }
};

Blockly.Blocks['ae_project_framescounttype_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("start frames count with")
        .appendField(new Blockly.FieldDropdown([["0","FramesCountType.FC_START_0"], ["1","FramesCountType.FC_START_1"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The Frame Count menu setting in the Project Settings dialog box.");
 this.setHelpUrl("guide/index.html#project-framescounttype");
  }
};

Blockly.Blocks['ae_project_framesusefeetframes_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("show frames as")
        .appendField(new Blockly.FieldDropdown([["feet + frames","true"], ["frames","false"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The Use Feet + Frames setting in the Project Settings dialog box. True if using Feet + Frames; false if using Frames.");
 this.setHelpUrl("guide/index.html#project-framesusefeetframes");
  }
};

Blockly.Blocks['ae_project_gpuacceltype_write'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set gpu accel type to")
        .appendField(new Blockly.FieldDropdown([["cuda","GpuAccelType.CUDA"], ["metal","GpuAccelType.Metal"], ["opencl","GpuAccelType.OPENCL"], ["software","GpuAccelType.SOFTWARE"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Get or set the current projects GPU Acceleration option.");
 this.setHelpUrl("guide/index.html#project-gpuacceltype");
  }
};

Blockly.Blocks['ae_project_items'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("all items in project");
    this.setOutput(true, "ItemCollection object");
    this.setColour(20);
 this.setTooltip("All of the items in the project.");
 this.setHelpUrl("guide/index.html#project-items");
  }
};

Blockly.Blocks['ae_project_linearblending'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["enable","true"], ["disable","false"]]), "TYPE")
        .appendField("linear blending");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("True if linear blending should be used for this project; otherwise false.");
 this.setHelpUrl("guide/index.html#project-linearblending");
  }
};

Blockly.Blocks['ae_project_numitems'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("number of items in project");
    this.setOutput(true, "Number");
    this.setColour(20);
 this.setTooltip("The total number of items contained in the project, including folders and all types of footage.");
 this.setHelpUrl("guide/index.html#project-numitems");
  }
};

Blockly.Blocks['ae_project_removeunusedfootage'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("remove unused footage");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Removes unused footage from the project. Same as the File Remove Unused Footage command.");
 this.setHelpUrl("guide/index.html#project-removeunusedfootage");
  }
};

Blockly.Blocks['ae_project_renderqueue'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("RenderQueueItem object")
        .appendField("render queue");
    this.setOutput(true, null);
    this.setColour(20);
 this.setTooltip("The renderqueue of the project.");
 this.setHelpUrl("guide/index.html#project-renderqueue");
  }
};

Blockly.Blocks['ae_project_renderqueue_write'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("RenderQueueItem object")
        .appendField("render queue");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The renderqueue of the project.");
 this.setHelpUrl("guide/index.html#project-renderqueue");
  }
};

Blockly.Blocks['ae_project_rootfolder'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("root folder");
    this.setOutput(true, "FolderItem");
    this.setColour(20);
 this.setTooltip("The root folder containing the contents of the project; this is a virtual folder that contains all items in the Project panel, but not items contained inside other folders in the Project panel.");
 this.setHelpUrl("guide/index.html#project-rootfolder");
  }
};

Blockly.Blocks['ae_project_rootfolder_write'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("root folder");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The root folder containing the contents of the project; this is a virtual folder that contains all items in the Project panel, but not items contained inside other folders in the Project panel.");
 this.setHelpUrl("guide/index.html#project-rootfolder");
  }
};

Blockly.Blocks['ae_project_selection'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("selection in project panel");
    this.setOutput(true, "Array");
    this.setColour(20);
 this.setTooltip("All items selected in the Project panel, in the sort order shown in the Project panel.");
 this.setHelpUrl("guide/index.html#project-selection");
  }
};

Blockly.Blocks['ae_project_timedisplaytype'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("display time as")
        .appendField(new Blockly.FieldDropdown([["frames","TimeDisplayType.FRAMES"], ["timecode","TimeDisplayType.TIMECODE"]]), "TYPE");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("The time display style, corresponding to the Time Display Style section in the Project Settings dialog box.");
 this.setHelpUrl("guide/index.html#project-timedisplaytype");
  }
};

Blockly.Blocks['ae_project_tooltype'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set tool type to")
        .appendField(new Blockly.FieldDropdown([["selection tool","ToolType.Tool_Arrow"], ["rotation tool","ToolType.Tool_Rotate"], ["unified camera tool","ToolType.Tool_CameraMaya"], ["orbit camera tool","ToolType.Tool_CameraOrbit"], ["track xy camera tool","ToolType.Tool_CameraTrackXY"], ["track z camera tool","ToolType.Tool_CameraTrackZ"], ["brush tool","ToolType.Tool_Paintbrush"], ["clone stamp tool","ToolType.Tool_CloneStamp"], ["eraser tool","ToolType.Tool_Eraser"], ["hand tool","ToolType.Tool_Hand"], ["zoom tool","ToolType.Tool_Magnify"], ["pan behind (anchor point) tool","ToolType.Tool_PanBehind"], ["rectangle tool","ToolType.Tool_Rect"], ["rounded rectangle tool","ToolType.Tool_RoundedRect"], ["ellipse tool","ToolType.Tool_Oval"], ["polygon tool","ToolType.Tool_Polygon"], ["star tool","ToolType.Tool_Star"], ["horizontal type tool","ToolType.Tool_TextH"], ["vertical type tool","ToolType.Tool_TextV"], ["pen tool","ToolType.Tool_Pen"], ["mask feather tool","ToolType.Tool_Feather"], ["add vertex tool","ToolType.Tool_PenPlus"], ["delete vertex tool","ToolType.Tool_PenMinus"], ["convert vertex tool","ToolType.Tool_PenConvert"], ["puppet pin tool","ToolType.Tool_Pin"], ["puppet starch tool","ToolType.Tool_PinStarch"], ["puppet overlap tool","ToolType.Tool_PinDepth"], ["roto brush tool","ToolType.Tool_Quickselect"], ["refine edge tool","ToolType.Tool_Hairbrush"]]), "TOOL");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Get and sets the active tool in the Tools panel.");
 this.setHelpUrl("guide/index.html#project-tooltype");
  }
};

Blockly.Blocks['ae_project_transparencygridthumbnails'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["show","TRUE"], ["hide","FALSE"]]), "TYPE")
        .appendField("transparency grid in thumbnails");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("When true, thumbnail views use the transparency checkerboard pattern.");
 this.setHelpUrl("guide/index.html#project-transparencygridthumbnails");
  }
};

Blockly.Blocks['ae_project_autofixexpressions'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("auto fix expressions");
    this.appendValueInput("OldText")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("old text");
    this.appendValueInput("NewText")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("new text");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Automatically replaces text found in broken expressions in the project, if the new text causes the expression to evaluate without errors.");
 this.setHelpUrl("guide/index.html#project-autofixexpressions");
  }
};

Blockly.Blocks['ae_project_close'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("close project and")
        .appendField(new Blockly.FieldDropdown([["prompt to save","CloseOptions.PROMPT_TO_SAVE_CHANGES"], ["save","CloseOptions.SAVE_CHANGES"], ["don't save","CloseOptions.DO_NOT_SAVE_CHANGES"]]), "NAME");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Closes the project with the option of saving changes automatically, prompting the user to save changes or closing without saving changes.");
 this.setHelpUrl("guide/index.html#project-close");
  }
};

Blockly.Blocks['ae_project_consolidatefootage'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("consolidate all footage");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Consolidates all footage in the project. Same as the File Consolidate All Footage command.");
 this.setHelpUrl("guide/index.html#project-consolidatefootage");
  }
};

Blockly.Blocks['ae_project_importfile'] = {
  init: function() {
    this.appendValueInput("IMPORT")
        .setCheck(null)
        .appendField("import file");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Imports the file specified in the specified ImportOptions object, using the specified options. Same as the File Import File command. Creates and returns a new FootageItem object from the file, and adds it to the project's items array.");
 this.setHelpUrl("guide/index.html#project-importfile");
  }
};

Blockly.Blocks['ae_project_importfilewithdialog'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("import file with dialog");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Shows an Import File dialog box. Same as the File Import > File command.");
 this.setHelpUrl("guide/index.html#project-importfilewithdialog");
  }
};

Blockly.Blocks['ae_project_importplaceholder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("create placeholder");
    this.appendValueInput("NAME")
        .setCheck(["Number", "String"])
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("width");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("FRAMERATE")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("frame rate");
    this.appendValueInput("DURATION")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("duration in seconds");
    this.setOutput(true, "PlaceholderItem");
    this.setColour(20);
 this.setTooltip("Creates and returns a new PlaceholderItem and adds it to the project's items array. Same as the File Import > Placeholder command.");
 this.setHelpUrl("guide/index.html#project-importplaceholder");
  }
};

Blockly.Blocks['ae_project_item'] = {
  init: function() {
    this.appendValueInput("NUMBER")
        .setCheck("Number")
        .appendField("project item nr");
    this.setInputsInline(true);
    this.setOutput(true, "Item");
    this.setColour(20);
 this.setTooltip("Retrieves an item at a specified index position.");
 this.setHelpUrl("guide/index.html#project-item");
  }
};

Blockly.Blocks['ae_project_itembyid'] = {
  init: function() {
    this.appendValueInput("ID")
        .setCheck("Number")
        .appendField("item by id");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(20);
 this.setTooltip("Retrieves an item by its Item ID");
 this.setHelpUrl("guide/index.html#project-itembyid");
  }
};

Blockly.Blocks['ae_project_reduceproject'] = {
  init: function() {
    this.appendValueInput("ITEMS")
        .setCheck(["Item", "Array"])
        .appendField("remove all items from project except");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Removes all items from the project except those specified. Same as the File Reduce Project command.");
 this.setHelpUrl("guide/index.html#project-reduceproject");
  }
};

Blockly.Blocks['ae_project_save'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("save project");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Saves the project. The same as the File Save or File > Save As command. If the project has never previously been saved and no file is specified, prompts the user for a location and file name. Pass a File object to save a project to a new file without prompting.");
 this.setHelpUrl("guide/index.html#project-save");
  }
};

Blockly.Blocks['ae_project_savewithdialog'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("save project with dialog");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Shows the Save dialog box. The user can name a file with a location and save the project, or click Cancel to exit the dialog box.");
 this.setHelpUrl("guide/index.html#project-savewithdialog");
  }
};

Blockly.Blocks['ae_project_showwindow'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["show","TRUE"], ["hide","FALSE"]]), "NAME")
        .appendField("project panel");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
 this.setTooltip("Shows or hides the Project panel.");
 this.setHelpUrl("guide/index.html#project-showwindow");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_project_items_addcomp'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pixelaspect = Blockly.JavaScript.valueToCode(block, 'PIXELASPECT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_duration = Blockly.JavaScript.valueToCode(block, 'DURATION', Blockly.JavaScript.ORDER_ATOMIC);
  var value_framerate = Blockly.JavaScript.valueToCode(block, 'FRAMERATE', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.project.items.addComp(' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_pixelaspect + ', ' + value_duration + ', ' + value_framerate + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_items_addfolder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.items.addFolder(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_activeitem'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.activeItem' + value_name + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_bitsperchannel'] = function(block) {
  var code = 'app.project.bitsPerChannel';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_bitsperchannel_write'] = function(block) {
  var dropdown_bpc = block.getFieldValue('bpc');
  var code = 'app.project.bitsPerChannel = ' + dropdown_bpc + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_feetframesfilmtype_write'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.displayStartFrame = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_file'] = function(block) {
  var value_type = Blockly.JavaScript.valueToCode(block, 'TYPE', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.file' + value_type;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};


Blockly.JavaScript['ae_project_file_write'] = function(block) {
  var value_type = Blockly.JavaScript.valueToCode(block, 'TYPE', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.file' + value_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_footagetimecodedisplaystarttype_write'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.footageTimecodeDisplayStartType = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_framescounttype_write'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.framesCountType = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_framesusefeetframes_write'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.framesUseFeetFrames = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_gpuacceltype_write'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.gpuAccelType = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_items'] = function(block) {
  var code = 'app.project.items';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_linearblending'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.linearBlending = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_numitems'] = function(block) {
  var code = 'app.project.numItems';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_removeunusedfootage'] = function(block) {
  var code = 'app.project.removeUnusedFootage();\n';
  return code;
};

Blockly.JavaScript['ae_project_renderqueue'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.renderQueue' + value_name;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_renderqueue_write'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.renderQueue' + value_name + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_rootfolder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.rootFolder' + value_name;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_rootfolder_write'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.rootFolder' + value_name + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_selection'] = function(block) {
  var code = 'app.project.selection';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_timedisplaytype'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.timeDisplayType = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_tooltype'] = function(block) {
  var dropdown_tool = block.getFieldValue('TOOL');
  var code = 'app.project.toolType = ' + dropdown_tool + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_transparencygridthumbnails'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = 'app.project.transparencyGridThumbnails = ' + dropdown_type + ';\n';
  return code;
};

Blockly.JavaScript['ae_project_autofixexpressions'] = function(block) {
  var value_oldtext = Blockly.JavaScript.valueToCode(block, 'OldText', Blockly.JavaScript.ORDER_ATOMIC);
  var value_newtext = Blockly.JavaScript.valueToCode(block, 'NewText', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.autoFixExpressions(' + value_oldtext + ', ' + value_newtext + ');\n';
  return code;
};

Blockly.JavaScript['ae_project_close'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  var code = 'app.project.close(' + dropdown_name + ');\n';
  return code;
};

Blockly.JavaScript['ae_project_consolidatefootage'] = function(block) {
  var code = 'app.project.consolidateFootage();\n';
  return code;
};

Blockly.JavaScript['ae_project_importfile'] = function(block) {
  var value_import = Blockly.JavaScript.valueToCode(block, 'IMPORT', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.importFile(' + value_import + ');\n';
  return code;
};

Blockly.JavaScript['ae_project_importfilewithdialog'] = function(block) {
  var code = 'app.project.importFileWithDialog();\n';
  return code;
};

Blockly.JavaScript['ae_project_importplaceholder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_framerate = Blockly.JavaScript.valueToCode(block, 'FRAMERATE', Blockly.JavaScript.ORDER_ATOMIC);
  var value_duration = Blockly.JavaScript.valueToCode(block, 'DURATION', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.project.importPlaceholder(' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_framerate + ', ' + value_duration + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_item'] = function(block) {
  var value_number = Blockly.JavaScript.valueToCode(block, 'NUMBER', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.item(' + value_number + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_itembyid'] = function(block) {
  var value_id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.itemByID(' + value_id + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_project_reduceproject'] = function(block) {
  var value_items = Blockly.JavaScript.valueToCode(block, 'ITEMS', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'app.project.reduceProject(' + value_items + ');\n';
  return code;
};

Blockly.JavaScript['ae_project_save'] = function(block) {
  var code = 'app.project.save();\n';
  return code;
};

Blockly.JavaScript['ae_project_savewithdialog'] = function(block) {
  var code = 'app.project.saveWithDialog();\n';
  return code;
};

Blockly.JavaScript['ae_project_showwindow'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  var code = 'app.project.showWindow(' + dropdown_name + ');\n';
  return code;
};