// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_folderitem_items'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".items");
    this.setOutput(true, "folderitem");
    this.setColour(70);
 this.setTooltip("An ItemCollection object containing Item object that represent the top-level contents of this folder. Unlike the ItemCollection in the Project object, this collection contains only the top-level items in the folder. Top-level within the folder is not the same as top-level within the project. Only those items that are top-level in the root folder are also top-level in the Project.");
 this.setHelpUrl("guide/index.html#folderitem-items");
  }
};

Blockly.Blocks['ae_folderitem_numitems'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".number of items");
    this.setOutput(true, "folderitem");
    this.setColour(70);
 this.setTooltip("The number of items contained in the items collection (``folderItem.items.length``). If the folder contains another folder, only the FolderItem for that folder is counted, not any subitems contained in it.");
 this.setHelpUrl("guide/index.html#folderitem-numitems");
  }
};

Blockly.Blocks['ae_folderitem_item'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".item");
    this.setInputsInline(true);
    this.setOutput(true, "folderitem");
    this.setColour(70);
 this.setTooltip("Returns the top-level item in this folder at the specified index position. Note that \"top-level\" here means toplevel within the folder, not necessarily within the project.");
 this.setHelpUrl("guide/index.html#folderitem-item");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_folderitem_items'] = function(block) {
  var code = '.items';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_folderitem_numitems'] = function(block) {
  var code = '.numItems';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_folderitem_item'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.item(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};