// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_avitem_duration'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".duration");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Returns the duration, in seconds, of the item. Still footage items have a duration of 0.");
    this.setHelpUrl("guide/index.html#avitem-duration");
  }
};

Blockly.Blocks['ae_avitem_footagemissing'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".footage missing");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("When true, the AVItem is a placeholder, or represents footage with a source file that cannot be found. In this case, the path of the missing source file is in the ``missingFootagePath`` attribute of the footage item's source-file object. See :ref:`FootageItem.mainSource` and :ref:`FileSource.missingFootagePath`.");
    this.setHelpUrl("guide/index.html#avitem-footagemissing");
  }
};

Blockly.Blocks['ae_avitem_frameduration'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".frame duration");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Returns the length of a frame for this AVItem, in seconds. This is the reciprocal of ``frameRate``. When set, the reciprocal is automatically set as a new ``frameRate`` value. This attribute returns the reciprocal of the ``frameRate``, which may not be identical to a value you set, if that value is not evenly divisible into 1.0 (for example, 0.3). Due to numerical limitations, (1 / (1 / 0.3)) is close to, but not exactly, 0.3. If the AVItem is a FootageItem, this value is linked to the ``mainSource``, and is read-only. To change it, set the ``conformFrameRate`` of the ``mainSource`` object. This sets both the ``frameRate`` and ``frameDuration`` of the FootageItem.");
    this.setHelpUrl("guide/index.html#avitem-frameduration");
  }
};

Blockly.Blocks['ae_avitem_framerate'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".frame rate");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The frame rate of the AVItem, in frames-per-second. This is the reciprocal of the ``frameDuration`` . When set, the reciprocal is automatically set as a new ``frameDuration`` value.");
    this.setHelpUrl("guide/index.html#avitem-framerate");
  }
};

Blockly.Blocks['ae_avitem_hasaudio'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".has audio");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("When true, the AVItem has an audio component.");
    this.setHelpUrl("guide/index.html#avitem-hasaudio");
  }
};

Blockly.Blocks['ae_avitem_hasvideo'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".has video");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("When true, the AVItem has an video component.");
    this.setHelpUrl("guide/index.html#avitem-hasvideo");
  }
};

Blockly.Blocks['ae_avitem_height'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".height");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The height of the item in pixels.");
    this.setHelpUrl("guide/index.html#avitem-height");
  }
};

Blockly.Blocks['ae_avitem_name'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("String")
      .appendField(".name");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The name of the item, as shown in the Project panel.");
    this.setHelpUrl("guide/index.html#avitem-name");
  }
};

Blockly.Blocks['ae_avitem_pixelaspect'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".pixel aspect");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The pixel aspect ratio (PAR) of the item.");
    this.setHelpUrl("guide/index.html#avitem-pixelaspect");
  }
};

Blockly.Blocks['ae_avitem_proxysource'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".proxy source");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The FootageSource being used as a proxy. The attribute is read-only; to change it, call any of the AVItem methods that change the proxy source: ``setProxy()``, ``setProxyWithSequence()``, ``setProxyWithSolid()``, or ``setProxyWithPlaceholder()``.");
    this.setHelpUrl("guide/index.html#avitem-proxysource");
  }
};

Blockly.Blocks['ae_avitem_time'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".time");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The current time of the item when it is being previewed directly from the Project panel. This value is a number of seconds. Use the global method :ref:`timeToCurrentFormat` to convert it to a string value that expresses the time in terms of frames. It is an error to set this value for a FootageItem whose ``mainSource`` is still (``item.mainSource.isStill`` is true).");
    this.setHelpUrl("guide/index.html#avitem-time");
  }
};

Blockly.Blocks['ae_avitem_usedin'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".used in");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("All the compositions that use this AVItem. Note that upon retrieval, the array value is copied, so it is not automatically updated. If you get this value, then add this item into another composition, you must retrieve the value again to get an array that includes the new item.");
    this.setHelpUrl("guide/index.html#avitem-usedin");
  }
};

Blockly.Blocks['ae_avitem_useproxy'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".use proxy");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("When true, a proxy is used for the item. It is set to true by all the ``SetProxy`` methods, and to false by the ``SetProxyToNone()`` method.");
    this.setHelpUrl("guide/index.html#avitem-useproxy");
  }
};

Blockly.Blocks['ae_avitem_width'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".width");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("The width of the item, in pixels.");
    this.setHelpUrl("guide/index.html#avitem-width");
  }
};

Blockly.Blocks['ae_avitem_setproxy'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck(["file", "String"])
      .appendField(".set proxy");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Sets a file as the proxy of this AVItem. Loads the specified file into a new FileSource object, sets this as thevalue of the ``proxySource`` attribute, and sets ``useProxy`` to true. It does not preserve the interpretation parameters, instead using the user preferences. If the file has an unlabeled alpha channel, and the user preference says to ask the user what to do, the method estimates the alpha interpretation, rather than asking the user. This differs from setting a FootageItem's ``mainSource``, but both actions are performed as in the user interface.");
    this.setHelpUrl("guide/index.html#avitem-setproxy");
  }
};

Blockly.Blocks['ae_avitem_setproxytonone'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set proxy to none");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Removes the proxy from this AVItem, sets the value of ``proxySource`` to ``null``, and sets the value of ``useProxy`` to false.");
    this.setHelpUrl("guide/index.html#avitem-setproxytonone");
  }
};

Blockly.Blocks['ae_avitem_setproxywithplaceholder'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set proxy with placeholder");
    this.appendValueInput("NAME")
      .setCheck("String")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("name");
    this.appendValueInput("WIDTH")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("width");
    this.appendValueInput("HEIGHT")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("height");
    this.appendValueInput("FRAMERATE")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("frame rate");
    this.appendValueInput("DURATION")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("duration");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Creates a PlaceholderSource object with specified values, sets this as the value of the ``proxySource`` attribute, and sets ``useProxy`` to true. It does not preserve the interpretation parameters, instead using the user preferences.");
    this.setHelpUrl("guide/index.html#avitem-setproxywithplaceholder");
  }
};

Blockly.Blocks['ae_avitem_setproxywithsequence'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("file")
      .appendField(".set proxy with sequence");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Sets a sequence of files as the proxy of this AVItem, with the option of forcing alphabetical order.");
    this.setHelpUrl("guide/index.html#avitem-setproxywithsequence");
  }
};

Blockly.Blocks['ae_avitem_setproxywithsolid'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set proxy with solid");
    this.appendValueInput("COLOR")
      .setCheck(null)
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("color");
    this.appendValueInput("NAME")
      .setCheck("String")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("name");
    this.appendValueInput("WIDTH")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("width");
    this.appendValueInput("HEIGHT")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("height");
    this.appendValueInput("PIXELASPECT")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("pixel aspect ratio");
    this.setOutput(true, "avitem");
    this.setColour(50);
    this.setTooltip("Creates a :ref:`SolidSource` with specified values, sets this as the value of the ``proxySource`` attribute, and sets ``useProxy`` to true. It does not preserve the interpretation parameters, instead using the user preferences.");
    this.setHelpUrl("guide/index.html#avitem-setproxywithsolid");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_avitem_duration'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.duration';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_footagemissing'] = function(block) {
  var code = '.footageMissing';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_frameduration'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.frameDuration';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_framerate'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.frameRate';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_hasaudio'] = function(block) {
  var code = '.hasAudio';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_hasvideo'] = function(block) {
  var code = '.hasVideo';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_height'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.height';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_name'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.name';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_pixelaspect'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.pixelAspect';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_proxysource'] = function(block) {
  var code = '.proxySource';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_time'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.time';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_usedin'] = function(block) {
  var code = '.usedIn';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_useproxy'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.useProxy';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_width'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.width';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_setproxy'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.setProxy(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_setproxytonone'] = function(block) {
  var code = '.setProxyToNone()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_setproxywithplaceholder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_framerate = Blockly.JavaScript.valueToCode(block, 'FRAMERATE', Blockly.JavaScript.ORDER_ATOMIC);
  var value_duration = Blockly.JavaScript.valueToCode(block, 'DURATION', Blockly.JavaScript.ORDER_ATOMIC);

  var code = '.setProxyWithPlaceholder(' + value_name + ', ' + value_width + ', ' + value_height + ' ,' + value_framerate + ' , ' + value_duration + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_setproxywithsequence'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);

  var code = '.setProxyWithSequence(' + value_name + ', true)';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avitem_setproxywithsolid'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'COLOR', Blockly.JavaScript.ORDER_ATOMIC);
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pixelaspect = Blockly.JavaScript.valueToCode(block, 'PIXELASPECT', Blockly.JavaScript.ORDER_ATOMIC);

  var code = '.setProxyWithSolid(' + value_color + ', ' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_pixelaspect + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};