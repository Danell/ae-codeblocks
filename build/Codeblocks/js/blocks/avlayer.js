// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_avlayer_adjustmentlayer'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".adjustment layer");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer is an adjustment layer.");
    this.setHelpUrl("guide/index.html#avlayer-adjustmentlayer");
  }
};

Blockly.Blocks['ae_avlayer_audioactive'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".audio active");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer's audio is active at the current time. For this value to be true, ``audioEnabled`` must be true, no other layer with audio may be soloing unless this layer is soloed too, and the time must be between the ``inPoint``");
    this.setHelpUrl("guide/index.html#avlayer-audioactive");
  }
};

Blockly.Blocks['ae_avlayer_audioenabled'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".audio enabled");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("When true, the layer's audio is enabled. This value corresponds to the audio toggle switch in the Timeline panel.");
    this.setHelpUrl("guide/index.html#avlayer-audioenabled");
  }
};

Blockly.Blocks['ae_avlayer_autoorient'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".auto orient")
      .appendField(new Blockly.FieldDropdown([
        ["along path", "AutoOrientType.ALONG_PATH"],
        ["camera or point of interest", "AutoOrientType.CAMERA_OR_POINT_OF_INTEREST"],
        ["characters toward camera", "AutoOrientType.CHARACTERS_TOWARD_CAMERA"],
        ["no auto orient", "AutoOrientType.NO_AUTO_ORIENT"]
      ]), "ORIENT");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The type of automatic orientation to perform for the layer.");
    this.setHelpUrl("guide/index.html#avlayer-autoorient");
  }
};

Blockly.Blocks['ae_avlayer_blendingmode'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set blending mode to")
      .appendField(new Blockly.FieldDropdown([
        ["normal", "BlendingMode.NORMAL"],
        ["dissolve", "BlendingMode.DISSOLVE"],
        ["dancing dissolve", "BlendingMode.DANCING_DISSOLVE"],
        ["darken", "BlendingMode."],
        ["multiply", "BlendingMode.MULTIPLY"],
        ["color burn", "BlendingMode.COLOR_BURN"],
        ["classic color burn", "BlendingMode.CLASSIC_COLOR_BURN"],
        ["linear burn", "BlendingMode.LINEAR_BURN"],
        ["darken color", "BlendingMode.DARKEN_COLOR"],
        ["add", "BlendingMode.ADD"],
        ["lighten", "BlendingMode.LIGHTEN"],
        ["screen", "BlendingMode.SCREEN"],
        ["color dodge", "BlendingMode.COLOR_DODGE"],
        ["classic color dodge", "BlendingMode.CLASSIC_COLOR_DODGE"],
        ["linear dodge", "BlendingMode.LINEAR_DODGE"],
        ["lighter color", "BlendingMode.LIGHTER_COLOR"],
        ["overlay", "BlendingMode.OVERLAY"],
        ["soft light", "BlendingMode.SOFT_LIGHT"],
        ["hard light", "BlendingMode.HARD_LIGHT"],
        ["linear light", "BlendingMode.LINEAR_LIGHT"],
        ["vivid light", "BlendingMode.VIVID_LIGHT"],
        ["pin light", "BlendingMode.PIN_LIGHT"],
        ["hard mix", "BlendingMode.HARD_MIX"],
        ["difference", "BlendingMode.DIFFERENCE"],
        ["classic difference", "BlendingMode.CLASSIC_DIFFERENCE"],
        ["exclusion", "BlendingMode.EXCLUSION"],
        ["subtrackt", "BlendingMode.SUBTRACKT"],
        ["divide", "BlendingMode.DIVIDE"],
        ["hue", "BlendingMode.HUE"],
        ["saturation", "BlendingMode.SATURATION"],
        ["color", "BlendingMode.COLOR"],
        ["luminosity", "BlendingMode.LUMINOSITY"],
        ["stencil alpha", "BlendingMode.STENCIL_ALPHA"],
        ["stencil luma", "BlendingMode.STENCIL_LUMA"],
        ["silhouette alpha", "BlendingMode.SILHOUETTE ALPHA"],
        ["silhouette luma", "BlendingMode.SILHOUETTE_LUMA"],
        ["alpha add", "BlendingMode.ALPHA_ADD"],
        ["luminescent premul", "BlendingMode.PREMUL"]
      ]), "BLENDING");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The blending mode of the layer.");
    this.setHelpUrl("guide/index.html#avlayer-blendingmode");
  }
};

Blockly.Blocks['ae_avlayer_cansetcollapsetransformation'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".can set collapse transformation");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if it is legal to change the value of the ``collapseTransformation`` attribute on this layer.");
    this.setHelpUrl("guide/index.html#avlayer-cansetcollapsetransformation");
  }
};

Blockly.Blocks['ae_avlayer_cansettimeremapenabled'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".can set time remap enabled");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if it is legal to change the value of the ``timeRemapEnabled`` attribute on this layer.");
    this.setHelpUrl("guide/index.html#avlayer-cansettimeremapenabled");
  }
};

Blockly.Blocks['ae_avlayer_collapsetransformation'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".collapse transformation");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if collapse transformation is on for this layer.");
    this.setHelpUrl("guide/index.html#avlayer-collapsetransformation");
  }
};

Blockly.Blocks['ae_avlayer_effectsactive'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".effects active");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer's effects are active, as indicated by the ``f`` icon next to it in the user interface.");
    this.setHelpUrl("guide/index.html#avlayer-effectsactive");
  }
};

Blockly.Blocks['ae_avlayer_environmentlayer'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".environment layer");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if this is an environment layer in a Ray-traced 3D composition. Setting this attribute to true automaticallymakes the layer 3D (``threeDLayer`` becomes true).");
    this.setHelpUrl("guide/index.html#avlayer-environmentlayer");
  }
};

Blockly.Blocks['ae_avlayer_frameblending'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".frame blending");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if frame blending is enabled for the layer.");
    this.setHelpUrl("guide/index.html#avlayer-frameblending");
  }
};

Blockly.Blocks['ae_avlayer_frameblendingtype'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".frame blending type")
      .appendField(new Blockly.FieldDropdown([
        ["frame mix", "FrameBlendingType.FRAME_MIX"],
        ["no frame blend", "FrameBlendingType.NO_FRAME_BLEND"],
        ["pixel motion", "FrameBlendingType.PIXEL_MOTION"]
      ]), "TYPE");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The type of frame blending to perform when frame blending is enabled for the layer.");
    this.setHelpUrl("guide/index.html#avlayer-frameblendingtype");
  }
};

Blockly.Blocks['ae_avlayer_guidelayer'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".guide layer");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer is a guide layer.");
    this.setHelpUrl("guide/index.html#avlayer-guidelayer");
  }
};

Blockly.Blocks['ae_avlayer_hasaudio'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".has audio");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer contains an audio component, regardless of whether it is audio-enabled or soloed.");
    this.setHelpUrl("guide/index.html#avlayer-hasaudio");
  }
};

Blockly.Blocks['ae_avlayer_hastrackmatte'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".has track matte");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer in front of this layer is being used as a track matte on this layer. When true, this layer's ``trackMatteType`` value controls how the matte is applied.");
    this.setHelpUrl("guide/index.html#avlayer-hastrackmatte");
  }
};

Blockly.Blocks['ae_avlayer_height'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".height");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The height of the layer in pixels.");
    this.setHelpUrl("guide/index.html#avlayer-height");
  }
};

Blockly.Blocks['ae_avlayer_isnamefromsource'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".is name from source");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if the layer has no expressly set name, but contains a named source. In this case, ``layer.name`` has the same value as ``layer.source.name``. False if the layer has an expressly set name, or if the layer does not have a source.");
    this.setHelpUrl("guide/index.html#avlayer-isnamefromsource");
  }
};

Blockly.Blocks['ae_avlayer_istrackmatte'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".is track matte");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if this layer is being used as a track matte for the layer behind it.");
    this.setHelpUrl("guide/index.html#avlayer-istrackmatte");
  }
};

Blockly.Blocks['ae_avlayer_motionblur'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".motion blur");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if motion blur is enabled for the layer.");
    this.setHelpUrl("guide/index.html#avlayer-motionblur");
  }
};

Blockly.Blocks['ae_avlayer_preservetransparency'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".preserve transparency");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if preserve transparency is enabled for the layer.");
    this.setHelpUrl("guide/index.html#avlayer-preservetransparency");
  }
};

Blockly.Blocks['ae_avlayer_quality'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set quality to")
      .appendField(new Blockly.FieldDropdown([
        ["best", "LayerQuality.BEST"],
        ["draft", "LayerQuality.DRAFT"],
        ["wireframe", "LayerQuality.WIREFRAME"]
      ]), "TYPE");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The quality with which this layer is displayed.");
    this.setHelpUrl("guide/index.html#avlayer-quality");
  }
};

Blockly.Blocks['ae_avlayer_source'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("avitem")
      .appendField(".source");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The source AVItem for this layer. The value is null in a Text layer. Use ``AVLayer.replaceSource()`` to change the value.");
    this.setHelpUrl("guide/index.html#avlayer-source");
  }
};

Blockly.Blocks['ae_avlayer_threedlayer'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".3D layer");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if this is a 3D layer.");
    this.setHelpUrl("guide/index.html#avlayer-threedlayer");
  }
};

Blockly.Blocks['ae_avlayer_threedperchar'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".3D per character");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("``True`` if this layer has the Enable Per-character 3D switch set, allowing its characters to be animated off the plane of the text layer. Applies only to text layers.");
    this.setHelpUrl("guide/index.html#avlayer-threedperchar");
  }
};

Blockly.Blocks['ae_avlayer_timeremapenabled'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".time remap enabled");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("True if time remapping is enabled for this layer.");
    this.setHelpUrl("guide/index.html#avlayer-timeremapenabled");
  }
};

Blockly.Blocks['ae_avlayer_trackmattetype'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set track matte type to")
      .appendField(new Blockly.FieldDropdown([
        ["alpha", "TrackMatteType.ALPHA"],
        ["inverted alpha", "TrackMatteType.ALPHA_INVERTED"],
        ["luma", "TrackMatteType.LUMA"],
        ["inverted luma", "TrackMatteType.LUMA_INVERTED"],
        ["no track matte", "TrackMatteType.NO_TRACK_MATTE"]
      ]), "MATTE");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("If this layer has a track matte, specifies the way the track matte is applied.");
    this.setHelpUrl("guide/index.html#avlayer-trackmattetype");
  }
};

Blockly.Blocks['ae_avlayer_width'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".width");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("The width of the layer in pixels.");
    this.setHelpUrl("guide/index.html#avlayer-width");
  }
};

Blockly.Blocks['ae_avlayer_audioactiveattime'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".audio active at time");
    this.setInputsInline(true);
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("Returns true if this layer's audio will be active at the specified time. For this method to return true, ``audioEnabled`` must be true, no other layer with audio may be soloing unless this layer is soloed too, and the time must be between the ``inPoint`` and ``outPoint`` of this layer.");
    this.setHelpUrl("guide/index.html#avlayer-audioactiveattime");
  }
};

Blockly.Blocks['ae_avlayer_calculatetransformfrompoints'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".calculate transform from points");
    this.appendValueInput("TOPLEFT")
      .setCheck("Array")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("point top left");
    this.appendValueInput("TOPRIGHT")
      .setCheck("Array")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("point top right");
    this.appendValueInput("BOTTOMRIGHT")
      .setCheck("Array")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("point bottom right");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("Calculates a transformation from a set of points in this layer.");
    this.setHelpUrl("guide/index.html#avlayer-calculatetransformfrompoints");
  }
};

Blockly.Blocks['ae_avlayer_openinviewer'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".open in viewer");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("Opens the layer in a Layer panel, and moves the Layer panel to front and gives it focus.");
    this.setHelpUrl("guide/index.html#avlayer-openinviewer");
  }
};

Blockly.Blocks['ae_avlayer_replacesource'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck(null)
      .appendField(".replace source");
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("Replaces the source for this layer.");
    this.setHelpUrl("guide/index.html#avlayer-replacesource");
  }
};

Blockly.Blocks['ae_avlayer_sourcerectattime'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".rectangle bounds at time");
    this.appendDummyInput()
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("include extents")
      .appendField(new Blockly.FieldCheckbox("TRUE"), "EXTENTS");
    this.setInputsInline(false);
    this.setOutput(true, "layercollection");
    this.setColour(100);
    this.setTooltip("Retrieves the rectangle bounds of the layer at the specified time index, corrected for text or shape layer content. Use, for example, to write text that is properly aligned to the baseline.");
    this.setHelpUrl("guide/index.html#avlayer-sourcerectattime");
  }
};


//    ####     #             #
//   #    #    #             #
//   #       #####   #    #  #####    #####
//    ####     #     #    #  #    #  #
//        #    #     #    #  #    #   ####
//   #    #    #     #   ##  #    #       #
//    ####      ###   ### #  #####   #####
//  

Blockly.JavaScript['ae_avlayer_adjustmentlayer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.adjustmentLayer';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_audioactive'] = function(block) {
  var code = '.audioActive';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_audioenabled'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.audioEnabled';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_autoorient'] = function(block) {
  var dropdown_orient = block.getFieldValue('ORIENT');
  var code = '.autoOrient';
  if (this.getChildren() != "") {
    code += " = " + dropdown_orient;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_blendingmode'] = function(block) {
  var dropdown_blending = block.getFieldValue('BLENDING');
  var code = '.blendingMode';
  if (this.getChildren() != "") {
    code += " = " + dropdown_orient;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_cansetcollapsetransformation'] = function(block) {
  var code = '.canSetCollapseTransformation';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_cansettimeremapenabled'] = function(block) {
  var code = '.canSetTimeRemapEnabled';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_collapsetransformation'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.collapseTransformation';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_effectsactive'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.effectsActive';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_environmentlayer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.environmentLayer';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_frameblending'] = function(block) {
  var code = '.frameBlending';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_frameblendingtype'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = '.frameBlendingType';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_guidelayer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.guideLayer';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_hasaudio'] = function(block) {
  var code = '.hasAudio';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_hastrackmatte'] = function(block) {
  var code = '.hasTrackMatte';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_height'] = function(block) {
  var code = '.height';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_isnamefromsource'] = function(block) {
  var code = '.isNameFromSource';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_istrackmatte'] = function(block) {
  var code = '.isTrackMatte';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_motionblur'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.motionBlur';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_preservetransparency'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.preserveTransparency';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_quality'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = '.quality';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_source'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.source' + value_name;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_threedlayer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.threeDLayer';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_threedperchar'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.threeDPerChar';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_timeremapenabled'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.timeRemapEnabled';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_trackmattetype'] = function(block) {
  var dropdown_matte = block.getFieldValue('MATTE');
  var code = '.trackMatteType';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_width'] = function(block) {
  var code = '.width';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_audioactiveattime'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.audioActiveAtTime(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_calculatetransformfrompoints'] = function(block) {
  var value_topleft = Blockly.JavaScript.valueToCode(block, 'TOPLEFT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_topright = Blockly.JavaScript.valueToCode(block, 'TOPRIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_bottomright = Blockly.JavaScript.valueToCode(block, 'BOTTOMRIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.calculateTransformFromPoints(' + value_topleft + ', ' + value_topright + ', ' + value_bottomright + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_openinviewer'] = function(block) {
  var code = '.openInViewer()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_replacesource'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.replaceSource(' + value_name + ', true)';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_avlayer_sourcerectattime'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var checkbox_extents = block.getFieldValue('EXTENTS') == 'TRUE';
  var code = '.sourceRectAtTime(' + value_name + ', ' + checkbox_extents + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};