// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_layer_active'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".active");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer's video is active at the current time. For this to be true, the layer must be enabled, no other layer may be soloing unless this layer is soloed too, and the time must be between the ``inPoint``");
 this.setHelpUrl("guide/index.html#layer-active");
  }
};

Blockly.Blocks['ae_layer_comment'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".comment");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("A descriptive comment for the layer.");
 this.setHelpUrl("guide/index.html#layer-comment");
  }
};

Blockly.Blocks['ae_layer_containingcomp'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".containing comp");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The composition that contains this layer.");
 this.setHelpUrl("guide/index.html#layer-containingcomp");
  }
};

Blockly.Blocks['ae_layer_enabled'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".enabled");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer is enabled; otherwise false. This corresponds to the video switch state of the layer in the Timeline panel.");
 this.setHelpUrl("guide/index.html#layer-enabled");
  }
};

Blockly.Blocks['ae_layer_hasvideo'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".has video");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer has a video switch (the eyeball icon) in the Timeline panel; otherwise false.");
 this.setHelpUrl("guide/index.html#layer-hasvideo");
  }
};

Blockly.Blocks['ae_layer_index'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".index");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The index position of the layer.");
 this.setHelpUrl("guide/index.html#layer-index");
  }
};

Blockly.Blocks['ae_layer_inpoint'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".in point");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The \"in\" point of the layer, expressed in composition time (seconds).");
 this.setHelpUrl("guide/index.html#layer-inpoint");
  }
};

Blockly.Blocks['ae_layer_isnameset'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".is name set");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("True if the value of the name attribute has been set explicitly, rather than automatically from the source.");
 this.setHelpUrl("guide/index.html#layer-isnameset");
  }
};

Blockly.Blocks['ae_layer_locked'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".locked");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer is locked; otherwise false. This corresponds to the lock toggle in the Layer panel.");
 this.setHelpUrl("guide/index.html#layer-locked");
  }
};

Blockly.Blocks['ae_layer_name'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".name");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The name of the layer. By default, this is the same as the Source name (which cannot be changed in the Layer panel), but you can set it to be different.");
 this.setHelpUrl("guide/index.html#layer-name");
  }
};

Blockly.Blocks['ae_layer_nulllayer'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".null layer");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer was created as a null object; otherwise false.");
 this.setHelpUrl("guide/index.html#layer-nulllayer");
  }
};

Blockly.Blocks['ae_layer_outpoint'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".out point");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The \"out\" point of the layer, expressed in composition time (seconds).");
 this.setHelpUrl("guide/index.html#layer-outpoint");
  }
};

Blockly.Blocks['ae_layer_parent'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("layer")
        .appendField(".parent");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The parent of this layer; can be null. Offset values are calculated to counterbalance any transforms above this layer in the hierarchy, so that when you set the parent there is no apparent jump in the layer's transform. For example, if the new parent has a rotation of 30 degrees, the child layer is assigned a rotation of -30 degrees. To set the parent without changing the child layer's transform values, use the :ref:`setParentWithJump layer.setParentWithJump` method.");
 this.setHelpUrl("guide/index.html#layer-parent");
  }
};

Blockly.Blocks['ae_layer_samplingquality'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".set sampling quality to")
        .appendField(new Blockly.FieldDropdown([["bicubic","LayerSamplingQuality.BICUBIC"], ["bilinear","LayerSamplingQuality.BILINEAR"]]), "TYPE");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Set/get layer sampling method (bicubic or bilinear)");
 this.setHelpUrl("guide/index.html#layer-samplingquality");
  }
};

Blockly.Blocks['ae_layer_selectedproperties'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".selected properties");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("An array containing all of the currently selected Property and PropertyGroup objects in the layer.");
 this.setHelpUrl("guide/index.html#layer-selectedproperties");
  }
};

Blockly.Blocks['ae_layer_shy'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".shy");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer is \"shy\", meaning that it is hidden in the Layer panel if the composition's \"Hide all shy layers\" option is toggled on.");
 this.setHelpUrl("guide/index.html#layer-shy");
  }
};

Blockly.Blocks['ae_layer_solo'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".solo");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("When true, the layer is soloed, otherwise false.");
 this.setHelpUrl("guide/index.html#layer-solo");
  }
};

Blockly.Blocks['ae_layer_starttime'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".start time");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The start time of the layer, expressed in composition time (seconds).");
 this.setHelpUrl("guide/index.html#layer-starttime");
  }
};

Blockly.Blocks['ae_layer_stretch'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".stretch");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The layer's time stretch, expressed as a percentage. A value of 100 means no stretch. Values between 0 and 1 are set to 1, and values between -1 and 0 (not including 0) are set to -1.");
 this.setHelpUrl("guide/index.html#layer-stretch");
  }
};

Blockly.Blocks['ae_layer_time'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".time");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("The current time of the layer, expressed in composition time (seconds).");
 this.setHelpUrl("guide/index.html#layer-time");
  }
};

Blockly.Blocks['ae_layer_activeattime'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".active at time");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Returns true if this layer will be active at the specified time. To return true, the layer must be enabled, no other layer may be soloing unless this layer is soloed too, and the time must be between the i n Poi nt and out Poi nt values of this layer.");
 this.setHelpUrl("guide/index.html#layer-activeattime");
  }
};

Blockly.Blocks['ae_layer_applypreset'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField(".apply preset");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Applies the specified collection of animation settings (an animation preset) to the layer. Predefined animation preset files are installed in the Presets folder, and users can create new animation presets through the user interface.");
 this.setHelpUrl("guide/index.html#layer-applypreset");
  }
};

Blockly.Blocks['ae_layer_copytocomp'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("compitem")
        .appendField(".copy to comp");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Copies the layer into the specified composition. The original layer remains unchanged. Creates a new Layer object with the same values as this one, and prepends the new object to the :ref:`layercollection` in the target CompItem. Retrieve the copy using into ``Comp.layer(1)``. Copying in a layer changes the index positions of previously existing layers in the target composition. This is the same as copying and pasting a layer through the user interface.");
 this.setHelpUrl("guide/index.html#layer-copytocomp");
  }
};

Blockly.Blocks['ae_layer_duplicate'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".duplicate");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Duplicates the layer. Creates a new Layer object in which all values are the same as in this one. This has the same effect as selecting a layer in the user interface and choosing Edit Duplicate, except the selection in the user interface does not change when you call this method.");
 this.setHelpUrl("guide/index.html#layer-duplicate");
  }
};

Blockly.Blocks['ae_layer_moveafter'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField(".move after");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Moves this layer to a position immediately after (below) the specified layer.");
 this.setHelpUrl("guide/index.html#layer-moveafter");
  }
};

Blockly.Blocks['ae_layer_movebefore'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField(".move before");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Moves this layer to a position immediately before (above) the specified layer.");
 this.setHelpUrl("guide/index.html#layer-movebefore");
  }
};

Blockly.Blocks['ae_layer_movetobeginning'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".move to beginning");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Moves this layer to the topmost position of the layer stack (the first layer).");
 this.setHelpUrl("guide/index.html#layer-movetobeginning");
  }
};

Blockly.Blocks['ae_layer_movetoend'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".move to end");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Moves this layer to the bottom position of the layer stack (the last layer).");
 this.setHelpUrl("guide/index.html#layer-movetoend");
  }
};

Blockly.Blocks['ae_layer_remove'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".remove");
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Deletes the specified layer from the composition.");
 this.setHelpUrl("guide/index.html#layer-remove");
  }
};

Blockly.Blocks['ae_layer_setparentwithjump'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("layer")
        .appendField(".set parent with jump");
    this.setInputsInline(true);
    this.setOutput(true, "layer");
    this.setColour(90);
 this.setTooltip("Sets the parent of this layer to the specified layer, without changing the transform values of the child layer. There may be an apparent jump in the rotation, translation, or scale of the child layer, as this layer's transform values are combined with those of its ancestors. If you do not want the child layer to jump, set the :ref:`parent layer.parent` attribute directly. In this case, an offset is calculated and set in the child layer's transform fields, to prevent the jump from occurring.");
 this.setHelpUrl("guide/index.html#layer-setparentwithjump");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_layer_active'] = function(block) {
  var code = '.active';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_comment'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.comment';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_containingcomp'] = function(block) {
  var code = '.containingComp';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_enabled'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.enabled';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_hasvideo'] = function(block) {
  var code = '.hasVideo';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_index'] = function(block) {
  var code = '.index';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_inpoint'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.inPoint';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_isnameset'] = function(block) {
  var code = '.isNameSet';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_locked'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.locked';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_name'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.name';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_nulllayer'] = function(block) {
  var code = '.nullLayer';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_outpoint'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.outPoint';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_parent'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.parent';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_samplingquality'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = '.samplingQuality';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_selectedproperties'] = function(block) {
  var code = '.selectedProperties';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_shy'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.shy';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_solo'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.solo';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_starttime'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.startTime';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_stretch'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.stretch';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_time'] = function(block) {
  var code = '.time';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_activeattime'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.activeAtTime(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_applypreset'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.applyPreset(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_copytocomp'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.copyToComp(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_duplicate'] = function(block) {
  var code = '.duplicate()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_moveafter'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.moveAfter(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_movebefore'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.moveBefore(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_movetobeginning'] = function(block) {
  var code = '.moveToBeginning()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_movetoend'] = function(block) {
  var code = '.moveToEnd()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_remove'] = function(block) {
  var code = '.remove()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layer_setparentwithjump'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.setParentWithJump(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};