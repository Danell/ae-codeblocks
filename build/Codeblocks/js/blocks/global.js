// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_clearoutput'] = {
  init: function() {
    this.appendDummyInput()
      .appendField("clear the info panel");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(10);
    this.setTooltip("Clears the output in the Info panel.");
    this.setHelpUrl("guide/index.html#clearoutput");
  }
};

Blockly.Blocks['ae_currentformattotime'] = {
  init: function() {
    this.appendDummyInput()
      .appendField("convert timecode to seconds");
    this.appendValueInput("timecode")
      .setCheck("String")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("timecode");
    this.appendValueInput("fps")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("fps");
    this.setOutput(true, "Number");
    this.setColour(10);
    this.setTooltip("Converts a formatted string for a frame time value to a number of seconds, given a specified frame rate. For example, if the formatted frame time value is 0:00:12 (the exact string format is determined by a project setting), and the frame rate is 24 fps, the time would be 0.5 seconds (12/24). If the frame rate is 30 fps, the time would be 0.4 seconds (12/30). If the time is a duration, the frames are counted from 0. Otherwise, the frames are counted from the project's starting frame (see :ref:`Project.displayStartFrame`).");
    this.setHelpUrl("guide/index.html#currentformattotime");
  }
};

Blockly.Blocks['ae_generaterandomnumber'] = {
  init: function() {
    this.appendDummyInput()
      .appendField("generates random number between 0 to 1");
    this.setOutput(true, "Number");
    this.setColour(10);
    this.setTooltip("Generates random numbers. This function is recommended instead of ``Math.random`` for generating random numbers that will be applied as values in a project (e.g., when using setValue).");
    this.setHelpUrl("guide/index.html#generaterandomnumber");
  }
};

Blockly.Blocks['ae_isvalid'] = {
  init: function() {
    this.appendDummyInput()
      .appendField("is object");
    this.appendValueInput("object")
      .setCheck(null);
    this.appendDummyInput()
      .appendField("still valid");
    this.setOutput(true, "Boolean");
    this.setColour(10);
    this.setTooltip("Determines if the specified After Effects object (e.g., composition, layer, mask, etc.) still exists. Some operations, such as :ref:`PropertyBase.moveTo`, might invalidate existing variable assignments to related objects. This function allows you to test whether those assignments are still valid before attempting to access them.");
    this.setHelpUrl("guide/index.html#isvalid");
  }
};

Blockly.Blocks['ae_timetocurrentformat'] = {
  init: function() {
    this.appendDummyInput()
      .appendField("convert seconds to timecode");
    this.appendValueInput("seconds")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("seconds");
    this.appendValueInput("fps")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("fps");
    this.setOutput(true, "String");
    this.setColour(10);
    this.setTooltip("Converts a numeric time value (a number of seconds) to a frame time value; that is, a formatted string thatshows which frame corresponds to that time, at the specified rate. For example, if the time is 0.5 seconds, andthe frame rate is 24 fps, the frame would be 0:00:12 (when the project is set to display as timecode). If the framerate is 30 fps, the frame would be 0:00:15. The format of the timecode string is determined by a project setting. If the time is a duration, the frames are counted from 0. Otherwise, the frames are counted from the project's starting frame (see :ref:`Project displayStartFrame project.displayStartFrame` attribute).");
    this.setHelpUrl("guide/index.html#timetocurrentformat");
  }
};

Blockly.Blocks['ae_writeln'] = {
  init: function() {
    this.appendValueInput("text")
      .setCheck(null)
      .appendField("write to info panel");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(10);
    this.setTooltip("Writes output to the Info panel and adds a line break at the end.");
    this.setHelpUrl("guide/index.html#writeln");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_clearoutput'] = function(block) {

  var code = 'clearOutput();\n';
  return code;
};

Blockly.JavaScript['ae_currentformattotime'] = function(block) {
  var value_timecode = Blockly.JavaScript.valueToCode(block, 'timecode', Blockly.JavaScript.ORDER_ATOMIC);
  var value_fps = Blockly.JavaScript.valueToCode(block, 'fps', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'currentFormatToTime(' + value_timecode + ', ' + value_fps + ')';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_generaterandomnumber'] = function(block) {

  var code = 'generateRandomNumber()';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_isvalid'] = function(block) {
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'isValid(' + value_object + ')';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_timetocurrentformat'] = function(block) {
  var value_seconds = Blockly.JavaScript.valueToCode(block, 'seconds', Blockly.JavaScript.ORDER_ATOMIC);
  var value_fps = Blockly.JavaScript.valueToCode(block, 'fps', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'timeToCurrentFormat(' + value_seconds + ', ' + value_fps + ')';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_writeln'] = function(block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'writeLn(' + value_text + ');\n';
  return code;
};