// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_system_machinename'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("machine name");
    this.setOutput(true, "String");
    this.setColour(30);
 this.setTooltip("The name of the computer on which After Effects is running.");
 this.setHelpUrl("guide/index.html#system-machinename");
  }
};

Blockly.Blocks['ae_system_osname'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("operating system name");
    this.setOutput(true, "String");
    this.setColour(30);
 this.setTooltip("The name of the operating system on which After Effects is running.");
 this.setHelpUrl("guide/index.html#system-osname");
  }
};

Blockly.Blocks['ae_system_osversion'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("operating system version");
    this.setOutput(true, "String");
    this.setColour(30);
 this.setTooltip("The version of the current local operating system.");
 this.setHelpUrl("guide/index.html#system-osversion");
  }
};

Blockly.Blocks['ae_system_username'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("system user name");
    this.setOutput(true, "String");
    this.setColour(30);
 this.setTooltip("The name of the user currently logged on to the system.");
 this.setHelpUrl("guide/index.html#system-username");
  }
};

Blockly.Blocks['ae_system_callsystem'] = {
  init: function() {
    this.appendValueInput("CALL")
        .setCheck("String")
        .appendField("call system");
    this.setOutput(true, "String");
    this.setColour(30);
 this.setTooltip("Executes a system command, as if you had typed it on the operating system's command line. Returns whatever the system outputs in response to the command, if anything. In Windows, you can invoke commands using the ``/c`` switch for the ``cmd.exe`` command, passing the command to run in escaped quotes (``\\\"...\\\"``). For example, the following retrieves the current time and displays it to the user::");
 this.setHelpUrl("guide/index.html#system-callsystem");
  }
};

Blockly.Blocks['ae_system_callsystem_write'] = {
  init: function() {
    this.appendValueInput("CALL")
        .setCheck("String")
        .appendField("call system");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
 this.setTooltip("Executes a system command, as if you had typed it on the operating system's command line. Returns whatever the system outputs in response to the command, if anything. In Windows, you can invoke commands using the ``/c`` switch for the ``cmd.exe`` command, passing the command to run in escaped quotes (``\\\"...\\\"``). For example, the following retrieves the current time and displays it to the user::");
 this.setHelpUrl("guide/index.html#system-callsystem");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_system_machinename'] = function(block) {
  var code = 'system.machineName';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_system_osname'] = function(block) {
  var code = '$.os';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_system_osversion'] = function(block) {
  var code = 'system.osVersion';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_system_username'] = function(block) {
  var code = 'system.userName';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_system_callsystem'] = function(block) {
  var value_call = Blockly.JavaScript.valueToCode(block, 'CALL', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'system.callSystem(' + value_call + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_system_callsystem_write'] = function(block) {
  var value_call = Blockly.JavaScript.valueToCode(block, 'CALL', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'system.callSystem(' + value_call + ');\n';
  return code;
};