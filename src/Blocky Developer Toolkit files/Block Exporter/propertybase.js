// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_propertybase_active'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".active");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is active. For a layer, this corresponds to the setting of the eyeball icon and if the current time is between the layer's in and out points. For an effect and all properties, it is the same as the e n abl e d attribute, except that it's read-only.");
 this.setHelpUrl("guide/index.html#propertybase-active");
  }
};

Blockly.Blocks['ae_propertybase_cansetenabled'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".can set enabled");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, you can set the ``enabled`` attribute value. Generally, this is true if the user interface displays an eyeball icon for this property; it is true for all layers.");
 this.setHelpUrl("guide/index.html#propertybase-cansetenabled");
  }
};

Blockly.Blocks['ae_propertybase_elided'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".elided");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is a group used to organize other properties. The property is not displayed in the user interface and its child properties are not indented in the Timeline panel.For example, for a text layer with two animators and no properties twirled down, you might see:");
 this.setHelpUrl("guide/index.html#propertybase-elided");
  }
};

Blockly.Blocks['ae_propertybase_enabled'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".enabled");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is enabled. It corresponds to the setting of the eyeball icon, if there is one; otherwise, the default is true.");
 this.setHelpUrl("guide/index.html#propertybase-enabled");
  }
};

Blockly.Blocks['ae_propertybase_iseffect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".is effect");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is an effect PropertyGroup.");
 this.setHelpUrl("guide/index.html#propertybase-iseffect");
  }
};

Blockly.Blocks['ae_propertybase_ismask'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".is mask");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is a mask PropertyGroup.");
 this.setHelpUrl("guide/index.html#propertybase-ismask");
  }
};

Blockly.Blocks['ae_propertybase_ismodified'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".is modified");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property has been changed since its creation.");
 this.setHelpUrl("guide/index.html#propertybase-ismodified");
  }
};

Blockly.Blocks['ae_propertybase_matchname'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".match name");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("A special name for the property used to build unique naming paths. The match name is not displayed, but you can refer to it in scripts. Every property has a unique match-name identifier. Match names are stable from version to version regardless of the display name (the name attribute value) or any changes to the application. Unlike the display name, it is not localized. An indexed group may not have a name value, but always has a matchName value. (An indexed group has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`.)");
 this.setHelpUrl("guide/index.html#propertybase-matchname");
  }
};

Blockly.Blocks['ae_propertybase_name'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".name");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("The display name of the property. (Compare :ref:`PropertyBase.matchName`.) It is an error to set the name value if the property is not a child of an indexed group (that is, a property group that has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`).");
 this.setHelpUrl("guide/index.html#propertybase-name");
  }
};

Blockly.Blocks['ae_propertybase_parentproperty'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".parent property");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("The property group that is the immediate parent of this property, or null if this PropertyBase is a layer.");
 this.setHelpUrl("guide/index.html#propertybase-parentproperty");
  }
};

Blockly.Blocks['ae_propertybase_propertydepth'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".property depth");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("The number of levels of parent groups between this property and the containing layer. The value 0 for a layer.");
 this.setHelpUrl("guide/index.html#propertybase-propertydepth");
  }
};

Blockly.Blocks['ae_propertybase_propertyindex'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".property index");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("The position index of this property within its parent group, if it is a child of an indexed group (a property group that has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`).");
 this.setHelpUrl("guide/index.html#propertybase-propertyindex");
  }
};

Blockly.Blocks['ae_propertybase_propertytype'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".set property type to")
        .appendField(new Blockly.FieldDropdown([["property","PropertyType.PROPERTY"], ["indexed group","PropertyType.INDEXED_GROUP"], ["named group","PropertyType.NAMED_GROUP"]]), "TYPE");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("The type of this property.");
 this.setHelpUrl("guide/index.html#propertybase-propertytype");
  }
};

Blockly.Blocks['ae_propertybase_selected'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Boolean")
        .appendField(".selected");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("When true, this property is selected. Set to true to select the property, or to false to deselect it. Sampling this attribute repeatedly for a large number of properties can slow down system performance. To read the full set of selected properties of a composition or layer, use either :ref:`CompItem.selectedProperties` or :ref:`Layer.selectedProperties`.");
 this.setHelpUrl("guide/index.html#propertybase-selected");
  }
};

Blockly.Blocks['ae_propertybase_duplicate'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".duplicate");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("If this property is a child of an indexed group, creates and returns a new PropertyBase object with the same attribute values as this one. If this property is not a child of an indexed group, the method generates an exception and displays an error. An indexed group has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`.");
 this.setHelpUrl("guide/index.html#propertybase-duplicate");
  }
};

Blockly.Blocks['ae_propertybase_moveto'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".move to index");
    this.setInputsInline(true);
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("Moves this property to a new position in its parent property group. This method is valid only for children of indexed groups; if it is not, or if the index value is not valid, the method generates an exception and displays an error. (An indexed group has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`.)");
 this.setHelpUrl("guide/index.html#propertybase-moveto");
  }
};

Blockly.Blocks['ae_propertybase_propertygroup'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".property group");
    this.appendValueInput("NAME")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("levels");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("Gets the PropertyGroup object for an ancestor group of this property at a specified level of the parent-child hierarchy.");
 this.setHelpUrl("guide/index.html#propertybase-propertygroup");
  }
};

Blockly.Blocks['ae_propertybase_remove'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".remove");
    this.setOutput(true, "propertybase");
    this.setColour(120);
 this.setTooltip("Removes this property from its parent group. If this is a property group, it removes the child properties as well. This method is valid only for children of indexed groups; if it is not, or if the index value is not valid, the method generates an exception and displays an error. (An indexed group has the type ``PropertyType.INDEXED_GROUP``; see :ref:`PropertyBase.propertyType`.) This method can be called on a text animation property (that is, any animator that has been set to a text layer).");
 this.setHelpUrl("guide/index.html#propertybase-remove");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_propertybase_active'] = function(block) {
   var code = '.active';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_cansetenabled'] = function(block) {
   var code = '.canSetEnabled';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_elided'] = function(block) {
   var code = '.elided';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_enabled'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
   var code = '.enabled';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_iseffect'] = function(block) {
   var code = '.isEffect';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_ismask'] = function(block) {
   var code = '.isMask';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_ismodified'] = function(block) {
   var code = '.isModified';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_matchname'] = function(block) {
   var code = '.matchName';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_name'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
   var code = '.name';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_parentproperty'] = function(block) {
   var code = '.parentProperty';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_propertydepth'] = function(block) {
   var code = '.propertyDepth';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_propertyindex'] = function(block) {
   var code = '.propertyIndex';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_propertytype'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
   var code = '.propertyType';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_selected'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
   var code = '.selected';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_duplicate'] = function(block) {
   var code = '.duplicate()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_moveto'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
   var code = '.moveTo(' + value_name + ')';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_propertygroup'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
   var code = '.propertyGroup(' + value_name + ')';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_propertybase_remove'] = function(block) {
   var code = '.remove()';
   return [code, Blockly.JavaScript.ORDER_ATOMIC];
};