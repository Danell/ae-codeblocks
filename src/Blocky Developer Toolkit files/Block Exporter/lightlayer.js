// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_lightlayer_lighttype'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".set light as")
        .appendField(new Blockly.FieldDropdown([["parallel","LightType.PARALLEL"], ["spot","LightType.SPOT"], ["point","LightType.POINT"], ["ambient","LightType.AMBIENT"]]), "TYPE");
    this.setOutput(true, "lightlayer");
    this.setColour(110);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_lightlayer_lighttype'] = function(block) {
  var dropdown_type = block.getFieldValue('TYPE');
  var code = '.lightType = ' + dropdown_type;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};