// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_item_item'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".item");
    this.setInputsInline(true);
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("The Item object represents an item that can appear in the Project panel. The first item is at index 1.");
    this.setHelpUrl("guide/index.html#item");
  }
};

Blockly.Blocks['ae_item_items'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".items");
    this.setInputsInline(true);
    this.setOutput(true, "Array");
    this.setColour(40);
    this.setTooltip("The Item object represents an item that can appear in the Project panel. The first item is at index 1.");
    this.setHelpUrl("guide/index.html#item");
  }
};

Blockly.Blocks['ae_item_comment'] = {
  init: function() {
    this.appendValueInput("TYPE")
      .setCheck("String")
      .appendField(".comment");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("A string that holds a comment, up to 15,999 bytes in length after any encoding conversion. The comment is for the user's purpose only; it has no effect on the item's appearance or behavior.");
    this.setHelpUrl("guide/index.html#item-comment");
  }
};

Blockly.Blocks['ae_item_id'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".id");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("A unique and persistent identification number used internally to identify an item between sessions. The value of the ID remains the same when the project is saved to a file and later reloaded. However, when you import this project into another project, new IDs are assigned to all items in the imported project. The ID is not displayed anywhere in the user interface.");
    this.setHelpUrl("guide/index.html#item-id");
  }
};

Blockly.Blocks['ae_item_label'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".label color");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("The label color for the item. Colors are represented by their number (0 for None, or 1 to 16 for one of the preset colors in the Labels preferences). Custom label colors cannot be set programmatically.");
    this.setHelpUrl("guide/index.html#item-label");
  }
};

Blockly.Blocks['ae_item_name'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("String")
      .appendField(".name");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("The name of the item as displayed in the Project panel.");
    this.setHelpUrl("guide/index.html#item-name");
  }
};

Blockly.Blocks['ae_item_parentfolder'] = {
  init: function() {
    this.appendValueInput("TYPE")
      .setCheck("folderitem")
      .appendField(".parent folder");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("The FolderItem object for the folder that contains this item. If this item is at the top level of the project, this is the project's root folder (``app.project.rootFolder``). You can use :ref:`ItemCollection.addFolder` to add a new folder, and set this value to put items in the new folder.");
    this.setHelpUrl("guide/index.html#item-parentfolder");
  }
};

Blockly.Blocks['ae_item_selected'] = {
  init: function() {
    this.appendValueInput("TYPE")
      .setCheck("Boolean")
      .appendField(".selected");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("When true, this item is selected. Multiple items can be selected at the same time. Set to true to select the item programmatically, or to false to deselect it.");
    this.setHelpUrl("guide/index.html#item-selected");
  }
};

Blockly.Blocks['ae_item_typename'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("String")
      .appendField(".type name");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("A user-readable name for the item type; for example, \"Folder\", \"Footage\", or \"Composition\".");
    this.setHelpUrl("guide/index.html#item-typename");
  }
};

Blockly.Blocks['ae_item_remove'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".remove");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("Deletes this item from the project and from the Project panel. If the item is a FolderItem, all the items contained in the folder are also removed from the project. No files or folders are removed from disk.");
    this.setHelpUrl("guide/index.html#item-remove");
  }
};

Blockly.Blocks['ae_item_label_write'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".set label color to")
      .appendField(new Blockly.FieldDropdown([
        [{ "src": "img/black.png", "width": 15, "height": 15, "alt": "none" }, "0"],
        [{ "src": "img/red.png", "width": 15, "height": 15, "alt": "red" }, "1"],
        [{ "src": "img/yellow.png", "width": 15, "height": 15, "alt": "yellow" }, "2"],
        [{ "src": "img/aqua.png", "width": 15, "height": 15, "alt": "aqua" }, "3"],
        [{ "src": "img/pink.png", "width": 15, "height": 15, "alt": "pink" }, "4"],
        [{ "src": "img/lavender.png", "width": 15, "height": 15, "alt": "lavendar" }, "5"],
        [{ "src": "img/peach.png", "width": 15, "height": 15, "alt": "peach" }, "6"],
        [{ "src": "img/seafoam.png", "width": 15, "height": 15, "alt": "sea foam" }, "7"],
        [{ "src": "img/blue.png", "width": 15, "height": 15, "alt": "blue" }, "8"],
        [{ "src": "img/green.png", "width": 15, "height": 15, "alt": "green" }, "9"],
        [{ "src": "img/purple.png", "width": 15, "height": 15, "alt": "purple" }, "10"],
        [{ "src": "img/orange.png", "width": 15, "height": 15, "alt": "orange" }, "11"],
        [{ "src": "img/brown.png", "width": 15, "height": 15, "alt": "brown" }, "12"],
        [{ "src": "img/fuchsia.png", "width": 15, "height": 15, "alt": "fuchsia" }, "13"],
        [{ "src": "img/cyan.png", "width": 15, "height": 15, "alt": "cyan" }, "14"],
        [{ "src": "img/sandstone.png", "width": 15, "height": 15, "alt": "sandstone" }, "15"],
        [{ "src": "img/darkgreen.png", "width": 15, "height": 15, "alt": "dark green" }, "16"]
      ]), "COLOR");
    this.setOutput(true, "item");
    this.setColour(40);
    this.setTooltip("The label color for the item. Colors are represented by their number (0 for None, or 1 to 16 for one of the preset colors in the Labels preferences). Custom label colors cannot be set programmatically.");
    this.setHelpUrl("guide/index.html#item-label");
  }
};


//    ####     #             #
//   #    #    #             #
//   #       #####   #    #  #####    #####
//    ####     #     #    #  #    #  #
//        #    #     #    #  #    #   ####
//   #    #    #     #   ##  #    #       #
//    ####      ###   ### #  #####   #####
//  

Blockly.JavaScript['ae_item_item'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.item(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_items'] = function(block) {
  var code = '.items';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_comment'] = function(block) {
  var value_type = Blockly.JavaScript.valueToCode(block, 'TYPE', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.comment';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_id'] = function(block) {
  var code = '.id';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_label'] = function(block) {
  var code = '.label';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_label_write'] = function(block) {
  var dropdown_color = block.getFieldValue('COLOR');
  var code = '.label = ' + dropdown_color;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_name'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.name';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_parentfolder'] = function(block) {
  var value_type = Blockly.JavaScript.valueToCode(block, 'TYPE', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.parentFolder';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_selected'] = function(block) {
  var value_type = Blockly.JavaScript.valueToCode(block, 'TYPE', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.selected';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_typename'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMICIC);
  var code = '.typeName';
  if (this.getChildren() != "") {
    code += " = " + value_type;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_item_remove'] = function(block) {
  var code = '.remove()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};