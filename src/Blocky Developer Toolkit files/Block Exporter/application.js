// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_app_activeviewer'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("current active viewer");
    this.setOutput(true, "viewer object");
    this.setColour(0);
 this.setTooltip("The Viewer object for the currently focused or active-focused viewer (Composition, Layer, or Footage) panel. Returns null if no viewers are open.");
 this.setHelpUrl("guide/index.html#app-activeviewer");
  }
};

Blockly.Blocks['ae_app_availablegpuacceltypes'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("available gpu accel types");
    this.setOutput(true, "Array");
    this.setColour(0);
 this.setTooltip("Get the available gpu accel types for the current system.");
 this.setHelpUrl("guide/index.html#app-availablegpuacceltypes");
  }
};

Blockly.Blocks['ae_app_buildname'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("after effects build name");
    this.setOutput(true, "String");
    this.setColour(0);
 this.setTooltip("The name of the build of After Effects being run, used internally by Adobe for testing and troubleshooting.");
 this.setHelpUrl("guide/index.html#app-buildname");
  }
};

Blockly.Blocks['ae_app_buildnumber'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("after effects build number");
    this.setOutput(true, "Number");
    this.setColour(0);
 this.setTooltip("The number of the build of After Effects being run, used internally by Adobe for testing and troubleshooting.");
 this.setHelpUrl("guide/index.html#app-buildnumber");
  }
};

Blockly.Blocks['ae_app_effects'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("all available effects");
    this.setOutput(true, "Array");
    this.setColour(0);
 this.setTooltip("The effects available in the application, with each element containing the following properties: displayName, category, matchName and version.");
 this.setHelpUrl("guide/index.html#app-effects");
  }
};

Blockly.Blocks['ae_app_isolanguage'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("iso language");
    this.setOutput(true, "String");
    this.setColour(0);
 this.setTooltip("A string indicating the locale (language and regional designations) After Effects is running.");
 this.setHelpUrl("guide/index.html#app-isolanguage");
  }
};

Blockly.Blocks['ae_app_isrenderengine'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("after effects running as a render engine");
    this.setOutput(true, "Boolean");
    this.setColour(0);
 this.setTooltip("True if After Effects is running as a render engine.");
 this.setHelpUrl("guide/index.html#app-isrenderengine");
  }
};

Blockly.Blocks['ae_app_iswatchfolder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("after effects watch folder active");
    this.setOutput(true, "Boolean");
    this.setColour(0);
 this.setTooltip("True if the Watch Folder dialog box is currently displayed and the application is currently watching a folder for rendering.");
 this.setHelpUrl("guide/index.html#app-iswatchfolder");
  }
};

Blockly.Blocks['ae_app_memoryinuse'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("bytes of memory currently used by after effects");
    this.setOutput(true, "Number");
    this.setColour(0);
 this.setTooltip("The number of bytes of memory currently used by this application.");
 this.setHelpUrl("guide/index.html#app-memoryinuse");
  }
};

Blockly.Blocks['ae_app_onerror_read'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("function set if error occurs");
    this.setOutput(true, "String");
    this.setColour(0);
 this.setTooltip("The name of a callback function that is called when an error occurs. By creating a function and assigning it to this attribute, you can respond to errors systematically; for example, you can close and restart the application, noting the error in a log file if it occurred during rendering. See :ref:`RenderQueue.render`. The callback function is passed the error string and a severity string. It should not return any value.");
 this.setHelpUrl("guide/index.html#app-onerror");
  }
};

Blockly.Blocks['ae_app_onerror_write'] = {
  init: function() {
    this.appendStatementInput("FUNC")
        .setCheck(null)
        .appendField("set function to run when error occurs");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("The name of a callback function that is called when an error occurs. By creating a function and assigning it to this attribute, you can respond to errors systematically; for example, you can close and restart the application, noting the error in a log file if it occurred during rendering. See :ref:`RenderQueue.render`. The callback function is passed the error string and a severity string. It should not return any value.");
 this.setHelpUrl("guide/index.html#app-onerror");
  }
};

Blockly.Blocks['ae_app_version'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("after effects version");
    this.setOutput(true, "String");
    this.setColour(0);
 this.setTooltip("An alphanumeric string indicating which version of After Effects is running.");
 this.setHelpUrl("guide/index.html#app-version");
  }
};

Blockly.Blocks['ae_app_beginsuppressdialogs'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("begin suppress error dialogs");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Begins suppression of script error dialog boxes in the user interface. Use `app.endSuppressDialogs()`_ to resume the display of error dialogs.");
 this.setHelpUrl("guide/index.html#app-beginsuppressdialogs");
  }
};

Blockly.Blocks['ae_app_beginundogroup'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("begin undo group with the name");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Marks the beginning of an undo group, which allows a script to logically group all of its actions as a single undoable action (for use with the Edit  Undo/Redo menu items). Use the `app.endUndoGroup()`_ method to mark the end of the group.");
 this.setHelpUrl("guide/index.html#app-beginundogroup");
  }
};

Blockly.Blocks['ae_app_canceltask'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField("cancel queued task with id");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Removes the specified task from the queue of tasks scheduled for delayed execution.");
 this.setHelpUrl("guide/index.html#app-canceltask");
  }
};

Blockly.Blocks['ae_app_endsuppressdialogs'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("end the suppression of error dialogs");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Ends the suppression of script error dialog boxes in the user interface. Error dialogs are displayed by default;call this method only if `app.beginSuppressDialogs()`_ has previously been called.");
 this.setHelpUrl("guide/index.html#app-endsuppressdialogs");
  }
};

Blockly.Blocks['ae_app_endundogroup'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("end undo group");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Marks the end of an undo group begun with the `app.beginUndoGroup()`_ method. You can use this method to place an end to an undo group in the middle of a script, should you wish to use more than one undo group for a single script. If you are using only a single undo group for a given script, you do not need to use this method; in its absence at the end of a script, the system will close the undo group automatically. Calling this method without having set a ``beginUndoGroup()`` method yields an error.");
 this.setHelpUrl("guide/index.html#app-endundogroup");
  }
};

Blockly.Blocks['ae_app_endwatchfolder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("end watch folder mode");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Ends Watch Folder mode.");
 this.setHelpUrl("guide/index.html#app-endwatchfolder");
  }
};

Blockly.Blocks['ae_app_newproject'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("create new project");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("skip saving")
        .appendField(new Blockly.FieldCheckbox("FALSE"), "DO_NOT_SAVE_CHANGES");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Creates a new project in After Effects, replicating the File  New > New Project menu command. If the current project has been edited, the user is prompted to save it. If the user cancels out of the Save dialog box, the new project is not created and the method returns null. Use ``app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES)`` to close the current project before opening a new one. See :ref:`project.close`");
 this.setHelpUrl("guide/index.html#app-newproject");
  }
};

Blockly.Blocks['ae_app_open'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("open project");
    this.appendValueInput("FILE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("(file)");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Opens a project.");
 this.setHelpUrl("guide/index.html#app-open");
  }
};

Blockly.Blocks['ae_app_pausewatchfolder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["pause","true"], ["unpause","false"]]), "PAUSE")
        .appendField("watch folder");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Pauses or resumes the search of the target watch folder for items to render.");
 this.setHelpUrl("guide/index.html#app-pausewatchfolder");
  }
};

Blockly.Blocks['ae_app_purge'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("purge")
        .appendField(new Blockly.FieldDropdown([["all caches","PurgeTarget.ALL_CACHES"], ["undo caches","PurgeTarget.UNDO_CACHES"], ["snapshot caches","PurgeTarget.SNAPSHOT_CACHES"], ["image caches","PurgeTarget.IMAGE_CACHES"]]), "NAME");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Purges unused data of the specified types from memory. Replicates the Purge options in the Edit menu.");
 this.setHelpUrl("guide/index.html#app-purge");
  }
};

Blockly.Blocks['ae_app_quit'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("quit after effects");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Quits the After Effects application.");
 this.setHelpUrl("guide/index.html#app-quit");
  }
};

Blockly.Blocks['ae_app_scheduletask'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField("schedule task");
    this.appendStatementInput("TASK")
        .setCheck(null);
    this.appendValueInput("DELAY")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("delay in milliseconds");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("repeat")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "REPEAT");
    this.setInputsInline(false);
    this.setOutput(true, "Number");
    this.setColour(0);
 this.setTooltip("Schedules the specified JavaScript for delayed execution.");
 this.setHelpUrl("guide/index.html#app-scheduletask");
  }
};

Blockly.Blocks['ae_app_watchfolder'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(["String", "folder object"])
        .appendField("start watch folder");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Starts a Watch Folder (network rendering) process pointed at a specified folder.");
 this.setHelpUrl("guide/index.html#app-watchfolder");
  }
};

Blockly.Blocks['ae_app_undogroup'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("undo group with name");
    this.appendStatementInput("FUNC")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Creates a complete undo group with beginning and end");
 this.setHelpUrl("guide/index.html#app-beginundogroup");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_app_activeviewer'] = function(block) {
  var code = 'app.activeViewer;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_availablegpuacceltypes'] = function(block) {
  var code = 'app.availableGPUAccelTypes;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_buildname'] = function(block) {
  var code = 'app.buildName;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_buildnumber'] = function(block) {
  var code = 'app.buildNumber;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_effects'] = function(block) {
  var code = 'app.effects;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_isolanguage'] = function(block) {
  var code = 'app.isoLanguage;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_isrenderengine'] = function(block) {
  var code = 'app.isRenderEngine;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_iswatchfolder'] = function(block) {
  var code = 'app.isWatchFolder;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_memoryinuse'] = function(block) {
  var code = 'app.memoryInUse;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_onerror_read'] = function(block) {
  var code = 'app.onError;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_onerror_write'] = function(block) {
  var statements_func = Blockly.JavaScript.statementToCode(block, 'FUNC');

  var code = 'app.onError = function(){\n' + statements_func + '}'
  return code;
};

Blockly.JavaScript['ae_app_version'] = function(block) {
  var code = 'app.version;';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_app_beginsuppressdialogs'] = function(block) {
  var code = 'app.beginSuppressDialogs();\n';
  return code;
};

Blockly.JavaScript['ae_app_beginundogroup'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.beginUndoGroup("' + value_name + '");\n';
  return code;
};

Blockly.JavaScript['ae_app_canceltask'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.cancelTask(' + value_name + ');\n';
  return code;
};

Blockly.JavaScript['ae_app_endsuppressdialogs'] = function(block) {
  var code = 'app.endSuppressDialogs();\n';
  return code;
};

Blockly.JavaScript['ae_app_endundogroup'] = function(block) {
  var code = 'app.endUndoGroup();\n';
  return code;
};

Blockly.JavaScript['ae_app_endwatchfolder'] = function(block) {
  var code = 'app.endWatchFolder();\n';
  return code;
};

Blockly.JavaScript['ae_app_newproject'] = function(block) {
  var checkbox_do_not_save_changes = block.getFieldValue('DO_NOT_SAVE_CHANGES') == 'TRUE';
  var code;
  if (checkbox_do_not_save_changes == true) {
	code = 'app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);\n\
app.newProject();\n'
  } else {
  	code = "app.newProject();\n";
  }
  return code;
};

Blockly.JavaScript['ae_app_open'] = function(block) {
  var value_file = Blockly.JavaScript.valueToCode(block, 'FILE', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.open(' + value_file + ');\n';
  return code;
};

Blockly.JavaScript['ae_app_pausewatchfolder'] = function(block) {
  var dropdown_pause = block.getFieldValue('PAUSE');

  var code = 'app.pauseWatchFolder(' + dropdown_pause + ');\n';
  return code;
};

Blockly.JavaScript['ae_app_purge'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');

  var code = 'app.purge(' + dropdown_name + ');\n';
  return code;
};

Blockly.JavaScript['ae_app_quit'] = function(block) {

  var code = 'app.quit();\n';
  return code;
};

Blockly.JavaScript['ae_app_scheduletask'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_task = Blockly.JavaScript.statementToCode(block, 'TASK');
  var value_delay = Blockly.JavaScript.valueToCode(block, 'DELAY', Blockly.JavaScript.ORDER_ATOMIC);
  var checkbox_repeat = block.getFieldValue('REPEAT') == 'TRUE';

  var code = 'app.scheduleTask(' + value_name + ', ' + value_delay + ', ' + checkbox_repeat + ');\n' + value_name + ' = (function() {\n' + statements_task + '})';
  return [code];
};

Blockly.JavaScript['ae_app_watchfolder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);

  var code = 'app.watchFolder(' + value_name + ');\n';
  return code;
};

Blockly.JavaScript['ae_app_undogroup'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_func = Blockly.JavaScript.statementToCode(block, 'FUNC');

  var code = 'app.beginUndoGroup(' + value_name + ');\n' + statements_func + "\napp.endUndoGroup();\n"
  return code;
};