// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_footageitem_file'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".file");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("The ExtendScript File object for the footage's source file. If the FootageItem's ``mainSource`` is a FileSource, this is the same as :ref:`FootageItem.mainSource.file FileSource.file`. Otherwise it is null.");
 this.setHelpUrl("guide/index.html#footageitem-file");
  }
};

Blockly.Blocks['ae_footageitem_mainsource'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".main source");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("The footage source, an object that contains all of the settings related to that footage item, including those that are normally accessed through the Interpret Footage dialog box. The attribute is read-only. To change its value, call one of the FootageItem \"replace\" methods. See the :ref:`FootageSource`, and its three types:");
 this.setHelpUrl("guide/index.html#footageitem-mainsource");
  }
};

Blockly.Blocks['ae_footageitem_openinviewer'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".open in viewer");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("Opens the footage in a Footage panel, and moves the Footage panel to front and gives it focus.");
 this.setHelpUrl("guide/index.html#footageitem-openinviewer");
  }
};

Blockly.Blocks['ae_footageitem_replace'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(["file", "String"])
        .appendField(".replace");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("Changes the source of this FootageItem to the specified file. In addition to loading the file, the method creates a new FileSource object for the file and sets mainSource to that object. In the new source object, it sets the ``name``, ``width``, ``height``, ``frameDuration``, and ``duration`` attributes (see :ref:`AVItem`) based on the contents of the file. The method preserves interpretation parameters from the previous ``mainSource`` object. If the specified file has an unlabeled alpha channel, the method estimates the alpha interpretation.");
 this.setHelpUrl("guide/index.html#footageitem-replace");
  }
};

Blockly.Blocks['ae_footageitem_replacewithplaceholder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".replace with placeholder");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("width");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("FRAMERATE")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("frame rate");
    this.appendValueInput("DURATION")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("duration in seconds");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("Changes the source of this FootageItem to the specified placeholder. Creates a new PlaceholderSource object, sets its values from the parameters, and sets ``mainSource`` to that object.");
 this.setHelpUrl("guide/index.html#footageitem-replacewithplaceholder");
  }
};

Blockly.Blocks['ae_footageitem_replacewithsequence'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(["file", "String"])
        .appendField(".replace with sequence");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("Changes the source of this FootageItem to the specified image sequence. In addition to loading the file, the method creates a new FileSource object for the file and sets ``mainSource`` to that object. In the new source object, it sets the ``name``, ``width``, ``height``, ``frameDuration``, and ``duration`` attributes (see :ref:`AVItem`) based on the contents of the file. The method preserves interpretation parameters from the previous ``mainSource`` object. If the specified file has an unlabeled alpha channel, the method estimates the alpha interpretation.");
 this.setHelpUrl("guide/index.html#footageitem-replacewithsequence");
  }
};

Blockly.Blocks['ae_footageitem_replacewithsolid'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".replace with solid");
    this.appendValueInput("COLOR")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("width");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("PIXELASPECT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("pixel aspect ratio");
    this.setOutput(true, "footageitem");
    this.setColour(80);
 this.setTooltip("Changes the source of this FootageItem to the specified solid. Creates a new SolidSource object, sets its values from the parameters, and sets ``mainSource`` to that object.");
 this.setHelpUrl("guide/index.html#footageitem-replacewithsolid");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_footageitem_file'] = function(block) {
  var code = '.items';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_mainsource'] = function(block) {
  var code = '.mainSource';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_openinviewer'] = function(block) {
  var code = '.openInViewer()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_replace'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.replace(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_replacewithplaceholder'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_framerate = Blockly.JavaScript.valueToCode(block, 'FRAMERATE', Blockly.JavaScript.ORDER_ATOMIC);
  var value_duration = Blockly.JavaScript.valueToCode(block, 'DURATION', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.replaceWithPlaceholder(' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_framerate + ', ' + value_duration + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_replacewithsequence'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.replaceWithSequence(' + value_name + ', true)';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_footageitem_replacewithsolid'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'COLOR', Blockly.JavaScript.ORDER_ATOMIC);
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pixelaspect = Blockly.JavaScript.valueToCode(block, 'PIXELASPECT', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.replaceWithSolid(' + value_color + ', ' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_pixelaspect + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};