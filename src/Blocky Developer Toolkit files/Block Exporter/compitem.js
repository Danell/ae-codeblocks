// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_compitem_activecamera'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".active camera");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The active camera, which is the front-most camera layer that is enabled. The value is null if the composition contains no enabled camera layers.");
    this.setHelpUrl("guide/index.html#compitem-activecamera");
  }
};

Blockly.Blocks['ae_compitem_bgcolor'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".background color");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The background color of the composition. The three array values specify the red, green, and blue components of the color.");
    this.setHelpUrl("guide/index.html#compitem-bgcolor");
  }
};

Blockly.Blocks['ae_compitem_displaystarttime'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".display start time");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The time set as the beginning of the composition, in seconds. This is the equivalent of the Start Timecode or Start Frame setting in the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-displaystarttime");
  }
};

Blockly.Blocks['ae_compitem_draft3d'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".draft 3d");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, Draft 3D mode is enabled for the Composition panel. This corresponds to the value of the Draft 3D button in the Composition panel.");
    this.setHelpUrl("guide/index.html#compitem-draft3d");
  }
};

Blockly.Blocks['ae_compitem_dropframe'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".drop frame");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, indicates that the composition uses drop-frame timecode. When false, indicates non-drop-frame timecode. This corresponds to the setting in the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-dropframe");
  }
};

Blockly.Blocks['ae_compitem_frameblending'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".frame blending");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, frame blending is enabled for this Composition. Corresponds to the value of the Frame Blending button in the Composition panel.");
    this.setHelpUrl("guide/index.html#compitem-frameblending");
  }
};

Blockly.Blocks['ae_compitem_frameduration'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".frame duration");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The duration of a frame, in seconds. This is the inverse of the ``frameRate`` value (frames-per-second).");
    this.setHelpUrl("guide/index.html#compitem-frameduration");
  }
};

Blockly.Blocks['ae_compitem_hideshylayers'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".hide shy layers");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, only layers with shy set to false are shown in the Timeline panel. When false, all layers are visible, including those whose shy value is true. Corresponds to the value of the Hide All Shy Layers button in the Composition panel.");
    this.setHelpUrl("guide/index.html#compitem-hideshylayers");
  }
};

Blockly.Blocks['ae_compitem_layers'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".layers");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("A :ref:`LayerCollection` that contains all the Layer objects for layers in this composition.");
    this.setHelpUrl("guide/index.html#compitem-layers");
  }
};

Blockly.Blocks['ae_compitem_motionblur'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".motion blur");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, motion blur is enabled for the composition. Corresponds to the value of the Motion Blur button in the Composition panel.");
    this.setHelpUrl("guide/index.html#compitem-motionblur");
  }
};

Blockly.Blocks['ae_compitem_motionbluradaptivesamplelimit'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".motion blur adaptive sample limit");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The maximum number of motion blur samples of 2D layer motion. This corresponds to the Adaptive Sample Limit setting in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-motionbluradaptivesamplelimit");
  }
};

Blockly.Blocks['ae_compitem_motionblursamplesperframe'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".motion blur samples per frame");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The minimum number of motion blur samples per frame for Classic 3D layers, shape layers, and certain effects. This corresponds to the Samples Per Frame setting in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-motionblursamplesperframe");
  }
};

Blockly.Blocks['ae_compitem_numlayers'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".num layers");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The number of layers in the composition.");
    this.setHelpUrl("guide/index.html#compitem-numlayers");
  }
};

Blockly.Blocks['ae_compitem_markerproperty'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("propertygroup")
      .appendField(".marker property");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("A :ref:`PropertyGroup` that contains all a composition's markers. Composition marker scripting has the same functionality as layer markers. See :ref:`MarkerValue`");
    this.setHelpUrl("guide/index.html#compitem-markerproperty");
  }
};

Blockly.Blocks['ae_compitem_openinviewer'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".open in viewer");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("Opens the composition in a Composition panel, and moves the Composition panel to front and gives it focus.");
    this.setHelpUrl("guide/index.html#compitem-openinviewer");
  }
};

Blockly.Blocks['ae_compitem_preservenestedframerate'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".preserve nested frame rate");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, the frame rate of nested compositions is preserved in the current composition. Corresponds to the value of the \"Preserve frame rate when nested or in render queue\" option in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-preservenestedframerate");
  }
};

Blockly.Blocks['ae_compitem_preservenestedresolution'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Boolean")
      .appendField(".preserve nested resolution");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("When true, the resolution of nested compositions is preserved in the current composition. Corresponds to the value of the \"Preserve Resolution When Nested\" option in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-preservenestedresolution");
  }
};

Blockly.Blocks['ae_compitem_renderer'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("String")
      .appendField(".renderer");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The current rendering plug-in module to be used to render this composition, as set in the Advanced tab of the Composition Settings dialog box. Allowed values are the members of :ref:`compItem.renderers`.");
    this.setHelpUrl("guide/index.html#compitem-renderer");
  }
};

Blockly.Blocks['ae_compitem_renderers'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".renderers");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The available rendering plug-in modules. Member strings reflect installed modules, as seen in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-renderers");
  }
};

Blockly.Blocks['ae_compitem_resolutionfactor'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Array")
      .appendField(".resolution factor");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The x and y downsample resolution factors for rendering the composition. The two values in the array specify how many pixels to skip when sampling; the first number controls horizontal sampling, the second controls vertical sampling. Full resolution is ``[1, 1]``, half resolution is ``[2, 2]``, and quarter resolution is ``[4, 4]``. The default is ``[1, 1]``.");
    this.setHelpUrl("guide/index.html#compitem-resolutionfactor");
  }
};

Blockly.Blocks['ae_compitem_selectedlayers'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".selected layers");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("All of the selected layers in this composition. This is a 0-based array (the first object is at index 0).");
    this.setHelpUrl("guide/index.html#compitem-selectedlayers");
  }
};

Blockly.Blocks['ae_compitem_selectedproperties'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".selected properties");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("All of the selected properties (Property and PropertyGroup objects) in this composition. The first property is at index position 0.");
    this.setHelpUrl("guide/index.html#compitem-selectedproperties");
  }
};

Blockly.Blocks['ae_compitem_shutterangle'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".shutter angle");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The shutter angle setting for the composition. This corresponds to the Shutter Angle setting in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-shutterangle");
  }
};

Blockly.Blocks['ae_compitem_shutterphase'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".shutter phase");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The shutter phase setting for the composition. This corresponds to the Shutter Phase setting in the Advanced tab of the Composition Settings dialog box.");
    this.setHelpUrl("guide/index.html#compitem-shutterphase");
  }
};

Blockly.Blocks['ae_compitem_workareaduration'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".work area duration");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The duration of the work area in seconds. This is the difference of the start-point and end-point times of the Composition work area.");
    this.setHelpUrl("guide/index.html#compitem-workareaduration");
  }
};

Blockly.Blocks['ae_compitem_workareastart'] = {
  init: function() {
    this.appendValueInput("NAME")
      .setCheck("Number")
      .appendField(".work area start");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("The time when the Composition work area begins, in seconds.");
    this.setHelpUrl("guide/index.html#compitem-workareastart");
  }
};

Blockly.Blocks['ae_compitem_duplicate'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".duplicate");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("Creates and returns a duplicate of this composition, which contains the same layers as the original.");
    this.setHelpUrl("guide/index.html#compitem-duplicate");
  }
};

Blockly.Blocks['ae_compitem_layer'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(".layer");
    this.appendValueInput("NAME")
      .setCheck("Number")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("by index");
    this.setOutput(true, "compitem");
    this.setColour(60);
    this.setTooltip("Returns a Layer object, which can be specified by name, an index position in this layer, or an index position relative to another layer.");
    this.setHelpUrl("guide/index.html#compitem-layer");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_compitem_activecamera'] = function(block) {
  var code = '.activeCamera';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_bgcolor'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'bgColor';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_displaystarttime'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.displayStartTime';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_draft3d'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.draft3d';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_dropframe'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.dropFrame';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_frameblending'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.frameBlending';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_frameduration'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.frameDuration';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_hideshylayers'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.hideShyLayers';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_layers'] = function(block) {
  var code = '.layers';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_motionblur'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.motionBlur';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_motionbluradaptivesamplelimit'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.motionBlurAdaptiveSampleLimit';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_motionblursamplesperframe'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.motionBlurSamplesPerFrame';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_numlayers'] = function(block) {
  var code = '.numLayers';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_markerproperty'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.markerProperty';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['ae_compitem_openinviewer'] = function(block) {
  var code = '.openInViewer()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_preservenestedframerate'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.preserveNestedFrameRate';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_preservenestedresolution'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.preserveNestedResolution';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_renderer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.renderer';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_renderers'] = function(block) {
  var code = '.renderers';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_resolutionfactor'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.resolutionFactor';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_selectedlayers'] = function(block) {
  var code = '.selectedLayers';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_selectedproperties'] = function(block) {
  var code = '.selectedProperties';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_shutterangle'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.shutterAngle';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_shutterphase'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.shutterPhase';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_workareaduration'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.workAreaDuration';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_workareastart'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.workAreaStart';
  if (this.getChildren() != "") {
    code += " = " + value_name;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_duplicate'] = function(block) {
  var code = '.duplicate()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_compitem_layer'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.layer(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};