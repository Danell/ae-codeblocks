// ####              ###     #               #       #       #
// #   #            #                                #
// #    #   ####   ####    ###     # ###   ###     #####   ###      ####   # ###    #####
// #    #  #    #   #        #     ##   #    #       #       #     #    #  ##   #  #
// #    #  ######   #        #     #    #    #       #       #     #    #  #    #   ####
// #   #   #        #        #     #    #    #       #       #     #    #  #    #       #
// ####     ####    #      #####   #    #  #####      ###  #####    ####   #    #  #####
//

Blockly.Blocks['ae_layercollection_add'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("avlayer")
        .appendField(".add");
    this.setInputsInline(true);
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new :ref:`AVLayer` containing the specified item, and adds it to this collection. The new layer honors the \"Create Layers at Composition Start Time\" preference. This method generates an exception if the item cannot be added as a layer to this CompItem.");
 this.setHelpUrl("guide/index.html#layercollection-add");
  }
};

Blockly.Blocks['ae_layercollection_addboxtext'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".add paragraph (box) text");
    this.setInputsInline(true);
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new paragraph (box) text layer and adds the new :ref:`TextLayer` to this collection. To create a point text layer, use the :ref:`LayerCollection.addText` method.");
 this.setHelpUrl("guide/index.html#layercollection-addboxtext");
  }
};

Blockly.Blocks['ae_layercollection_addcamera'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".add camera");
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new camera layer and adds the :ref:`CameraLayer` to this collection. The new layer honors the \"Create Layers at Composition Start Time\" preference.");
 this.setHelpUrl("guide/index.html#layercollection-addcamera");
  }
};

Blockly.Blocks['ae_layercollection_addlight'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".add light");
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new light layer and adds the :ref:`LightLayer` to this collection. The new layer honors the \"Create Layers at Composition Start Time\" preference.");
 this.setHelpUrl("guide/index.html#layercollection-addlight");
  }
};

Blockly.Blocks['ae_layercollection_addnull'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField(".add null");
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new null layer and adds the :ref:`AVLayer` to this collection. This is the same as choosing Layer  New > Null Object.");
 this.setHelpUrl("guide/index.html#layercollection-addnull");
  }
};

Blockly.Blocks['ae_layercollection_addshape'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".add shape");
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new :ref:`ShapeLayer` for a new, empty Shape layer. Use the ShapeLayer object to add properties, such as shape, fill, stroke, and path filters. This is the same as using a shape tool in \"Tool Creates Shape\" mode. Tools automatically add a vector group that includes Fill and Stroke as specified in the tool options.");
 this.setHelpUrl("guide/index.html#layercollection-addshape");
  }
};

Blockly.Blocks['ae_layercollection_addsolid'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".add solid");
    this.appendValueInput("COLOR")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendValueInput("WIDTH")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("width");
    this.appendValueInput("HEIGHT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("PIXELASPECT")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("pixel aspect ratio");
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new :ref:`SolidSource`, with values set as specified; sets the new SolidSource as the ``mainSource`` value of a new :ref:`FootageItem`, and adds the FootageItem to the project. Creates a new :ref:`AVLayer`, sets the new Footage Item as its ``source``, and adds the layer to this collection.");
 this.setHelpUrl("guide/index.html#layercollection-addsolid");
  }
};

Blockly.Blocks['ae_layercollection_addtext'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".add text");
    this.setInputsInline(true);
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new point text layer and adds the new :ref:`TextLayer` to this collection. To create a paragraph (box) text layer, use :ref:`LayerCollection.addBoxText`.");
 this.setHelpUrl("guide/index.html#layercollection-addtext");
  }
};

Blockly.Blocks['ae_layercollection_byname'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("String")
        .appendField(".find layer by name");
    this.setInputsInline(true);
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Returns the first (topmost) layer found in this collection with the specified name, or null if no layer with the given name is found.");
 this.setHelpUrl("guide/index.html#layercollection-byname");
  }
};

Blockly.Blocks['ae_layercollection_precompose'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(".precompose");
    this.appendValueInput("LAYERS")
        .setCheck("Array")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("layer indexs");
    this.appendValueInput("NAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("name");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("move attributes")
        .appendField(new Blockly.FieldCheckbox("FALSE"), "MOVEALLATTRIBUTES");
    this.setInputsInline(false);
    this.setOutput(true, "layercollection");
    this.setColour(100);
 this.setTooltip("Creates a new :ref:`CompItem` and moves the specified layers into its layer collection. It removes the individual layers from this collection, and adds the new CompItem to this collection.");
 this.setHelpUrl("guide/index.html#layercollection-precompose");
  }
};


// 	  ####     #             #
// 	 #    #    #             #
// 	 #       #####   #    #  #####    #####
// 	  ####     #     #    #  #    #  #
// 	      #    #     #    #  #    #   ####
// 	 #    #    #     #   ##  #    #       #
// 	  ####      ###   ### #  #####   #####
// 	

Blockly.JavaScript['ae_layercollection_add'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.add(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addboxtext'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addBoxText(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addcamera'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addCamera(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addlight'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addLight(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addnull'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addNull(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addshape'] = function(block) {
  var code = '.addShape()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_addsolid'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'COLOR', Blockly.JavaScript.ORDER_ATOMIC);
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_width = Blockly.JavaScript.valueToCode(block, 'WIDTH', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'HEIGHT', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pixelaspect = Blockly.JavaScript.valueToCode(block, 'PIXELASPECT', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addSolid(' + value_color + ', ' + value_name + ', ' + value_width + ', ' + value_height + ', ' + value_pixelaspect + ')';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['ae_layercollection_addtext'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.addText(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_byname'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '.byName(' + value_name + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ae_layercollection_precompose'] = function(block) {
  var value_layers = Blockly.JavaScript.valueToCode(block, 'LAYERS', Blockly.JavaScript.ORDER_ATOMIC);
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var checkbox_moveallattributes = block.getFieldValue('MOVEALLATTRIBUTES') == 'TRUE';
  var code = '.precompose(' + value_layers + ', ' + value_name + ', ' + checkbox_moveallattributes + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};