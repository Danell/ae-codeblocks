var processFile = File.openDialog("Select file to process.");
var name, tooltip, helpurl, text, returnType;
var xmlArray = [];
var firstRun = true;
var readWrite = false;
var read = false;
var readWriteProcess = false;
var hue = "130";
var dummyBlocks = false;
var staticReturnValue = true;
var staticReturnType = "property";
var preText = ".";

if (processFile != null) {
  processFile.open("r");
  var txt;
  var descriptionCouter = 0;
  var returnCouter = 0;

  while (!processFile.eof) {
    txt = processFile.readln();

    //Start of object
    if (txt.indexOf(".. _") + 1) {
      if (!firstRun) {
        if (readWrite && readWriteProcess) {
          addToArray("read");
          addToArray("write");
        } else if (read) {
          addToArray("read");
        } else {
          addToArray("write");
        }
        name = tooltip = helpurl = text = returnType = "";
        readWrite = read = false;
        descriptionCouter = 0;
      } else {
        firstRun = false;
      }
      name = "ae_" + txt.substr(4, txt.length - 5);
      text = txt.substr(4, txt.length - 5).split(".").pop().replace(/([A-Z])/g, ' $1').toLowerCase();
      helpurl = "guide/index.html#" + txt.substr(4, txt.length - 5).replace(".", "-").toLowerCase();
      continue;
    }

    //Decrition
    if (descriptionCouter == 1) {
      descriptionCouter = 2
    } else if (descriptionCouter == 2) {
      tooltip = txt.replace("<", "").replace(">", "").replace("&", "and");
      descriptionCouter = 0;
    }
    if (txt.indexOf("**Description**") + 1) {
      descriptionCouter = 1;
    }

    //Return type
    if (returnCouter == 1) {
      returnCouter = 2
    } else if (returnCouter == 2) {
      txt = txt.toLowerCase();
      if (txt.toLowerCase().indexOf("nothing") + 1) {
        returnType = '<block type="type_null" id=""></block>';

      } else if (txt.indexOf("floating-point") + 1 || txt.indexOf("integer") + 1) {
        returnType = '<block type="type_number" id=""></block>';

      } else if (txt.indexOf("boolean") + 1) {
        returnType = '<block type="type_boolean" id=""></block>';

      } else if (txt.indexOf("string") + 1) {
        returnType = '<block type="type_string" id=""></block>';

      } else if (txt.indexOf("array") + 1) {
        returnType = '<block type="type_list" id=""></block>';

      } else {
        returnType = '<block type="type_other" id="">\
  <field name="TYPE">' + txt + '</field>\
</block>';
      }

      returnCouter = 0;
    }
    if (txt.indexOf("**Returns**") + 1 || txt.indexOf("**Type**") + 1) {
      returnCouter = 1;
    }

    //Read/Write type
    if (txt.toLowerCase().indexOf("read/write") + 1) {
      readWrite = true;
    } else if (txt.toLowerCase().indexOf("read-only") + 1 || txt.toLowerCase().indexOf("Nothing.") + 1) {
      read = true;
    }
  }

  //Run last caught info
  if (readWrite && readWriteProcess) {
    addToArray("read");
    addToArray("write");
  } else if (read) {
    addToArray("read");
  } else {
    addToArray("write");
  }

  alert(
    '<xml xmlns="http://www.w3.org/1999/xhtml">\n' + xmlArray.join("\n") + '\n</xml>'
  )

}

function addToArray(type) {
  var connections, output;
  var retrunValue = returnType;
  if (staticReturnValue) {
    retrunValue = '<block type="type_other" id="">\
  <field name="TYPE">' + staticReturnType + '</field>\
</block>';
  }

  if (type == "write" && readWriteProcess) {
    connections = "BOTH";
    output = '    <value name="TOPTYPE">\
      <shadow type="type_null" id=""></shadow>\
    </value>\
    <value name="BOTTOMTYPE">\
      <shadow type="type_null" id=""></shadow>\
    </value>';
  } else {
    connections = "LEFT";
    output = '    <value name="OUTPUTTYPE">\
      <shadow type="type_null" id=""></shadow>\
      ' + retrunValue + '\
    </value>';
  }

  var blockType, blockInfo;
  if (type == "read") {
    blockType = '<block type="input_dummy" id="">';
    blockInfo = "";
  } else {
    blockType = '<block type="input_value" id="">';
    blockInfo = '<value name="TYPE">\
        <shadow type="type_null" id=""></shadow>\
          ' + returnType + '\
      </value>';
  }

  if (!readWriteProcess) {
    type = "";
  }

  xmlArray.push(
    '<block type="factory_base" id="" deletable="false" movable="false" x="0" y="0">\
    <mutation connections="' + connections + '"></mutation>\
    <field name="NAME">' + type + "" + name + '</field>\
    <field name="INLINE">AUTO</field>\
    <field name="CONNECTIONS">' + connections + '</field>\
    <statement name="INPUTS">\
      ' + blockType + '\
        <field name="ALIGN">LEFT</field>\
        <statement name="FIELDS">\
          <block type="field_static" id="">\
            <field name="TEXT">' + preText + text.toLowerCase() + '</field>\
          </block>\
        </statement>\
        ' + blockInfo + '\
      </block>\
    </statement>\
    <value name="TOOLTIP">\
      <block type="text" id="" deletable="false" movable="false">\
        <field name="TEXT">' + tooltip + '</field>\
      </block>\
    </value>\
    <value name="HELPURL">\
      <block type="text" id="" deletable="false" movable="false">\
        <field name="TEXT">' + helpurl + '</field>\
      </block>\
    </value>' +
    output +
    '    <value name="COLOUR">\
      <block type="colour_hue" id="">\
        <mutation colour="#5b67a5"></mutation>\
        <field name="HUE">' + hue + '</field>\
      </block>\
    </value>\
  </block>'
  );
}