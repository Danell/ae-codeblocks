Welcome to the After Effects Scripting Guide!
---------------------------------------------

.. toctree::
    :maxdepth: 1
    :caption: Introduction

    overview
    changelog
    javascript
    objectmodel

.. toctree::
    :maxdepth: 1
    :caption: General

    globals
    application
    project
    system

.. toctree::
    :maxdepth: 1
    :caption: Items

    item
    itemcollection
    avitem
    compitem
    folderitem
    footageitem

.. toctree::
    :maxdepth: 1
    :caption: Layers

    layer
    layercollection
    avlayer
    cameralayer
    lightlayer
    shapelayer
    textlayer

.. toctree::
    :maxdepth: 1
    :caption: Properties

    propertybase
    property
    propertygroup
    maskpropertygroup

.. toctree::
    :maxdepth: 1
    :caption: Renderqueue

    renderqueue
    rqitemcollection
    renderqueueitem
    omcollection
    outputmodule

.. toctree::
    :maxdepth: 1
    :caption: Sources

    filesource
    footagesource
    placeholdersource
    solidsource

.. toctree::
    :maxdepth: 1
    :caption: Other

    collection
    importoptions
    keyframeease
    markervalue
    settings
    shape
    textdocument
    viewer
